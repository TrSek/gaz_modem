<?php $dp_table = array (
  0 => 
  array (
    'mark' => 'Vb',
    'unit' => 'm3',
    'exponent' => 0,
    'variable' => 9,
    'info' => 217,
  ),
  1 => 
  array (
    'mark' => 'Vm',
    'unit' => 'm3',
    'exponent' => 0,
    'variable' => 9,
    'info' => 121,
  ),
  2 => 
  array (
    'mark' => 'V2',
    'unit' => 'm3',
    'exponent' => 0,
    'variable' => 9,
    'info' => 25,
  ),
  3 => 
  array (
    'mark' => 'E',
    'unit' => 'kWh',
    'exponent' => 0,
    'variable' => 9,
    'info' => 217,
  ),
  4 => 
  array (
    'mark' => 'M',
    'unit' => 'kg',
    'exponent' => 0,
    'variable' => 9,
    'info' => 25,
  ),
  5 => 
  array (
    'mark' => 'Qb',
    'unit' => 'm3/h',
    'exponent' => 0,
    'variable' => 7,
    'info' => 23,
  ),
  6 => 
  array (
    'mark' => 'Qm',
    'unit' => 'm3/h',
    'exponent' => 0,
    'variable' => 7,
    'info' => 23,
  ),
  7 => 
  array (
    'mark' => 'QE',
    'unit' => 'kW',
    'exponent' => 0,
    'variable' => 7,
    'info' => 23,
  ),
  8 => 
  array (
    'mark' => 'QM',
    'unit' => 'kg/h',
    'exponent' => 0,
    'variable' => 7,
    'info' => 23,
  ),
  9 => 
  array (
    'mark' => 'dVb',
    'unit' => 'm3',
    'exponent' => 0,
    'variable' => 9,
    'info' => 217,
  ),
  10 => 
  array (
    'mark' => 'dVm',
    'unit' => 'm3',
    'exponent' => 0,
    'variable' => 9,
    'info' => 89,
  ),
  11 => 
  array (
    'mark' => 'dV2',
    'unit' => 'm3',
    'exponent' => 0,
    'variable' => 9,
    'info' => 25,
  ),
  12 => 
  array (
    'mark' => 'dE',
    'unit' => 'kWh',
    'exponent' => 0,
    'variable' => 9,
    'info' => 217,
  ),
  13 => 
  array (
    'mark' => 'dM',
    'unit' => 'kg',
    'exponent' => 0,
    'variable' => 9,
    'info' => 25,
  ),
  14 => 
  array (
    'mark' => 'dLF',
    'unit' => 'imp',
    'exponent' => 0,
    'variable' => 6,
    'info' => 22,
  ),
  15 => 
  array (
    'mark' => 'dHF',
    'unit' => 'imp',
    'exponent' => 0,
    'variable' => 6,
    'info' => 22,
  ),
  16 => 
  array (
    'mark' => 'p1',
    'unit' => 'kPa',
    'exponent' => 0,
    'variable' => 7,
    'info' => 87,
  ),
  17 => 
  array (
    'mark' => 't',
    'unit' => '°C',
    'exponent' => 0,
    'variable' => 7,
    'info' => 87,
  ),
  18 => 
  array (
    'mark' => 'p2',
    'unit' => 'kPa',
    'exponent' => 0,
    'variable' => 7,
    'info' => 87,
  ),
  19 => 
  array (
    'mark' => 'R_Pt100',
    'unit' => 'Ohm',
    'exponent' => 0,
    'variable' => 7,
    'info' => 23,
  ),
  20 => 
  array (
    'mark' => 'tamb',
    'unit' => '°C',
    'exponent' => 0,
    'variable' => 7,
    'info' => 87,
  ),
  21 => 
  array (
    'mark' => 'Batt.',
    'unit' => '%',
    'exponent' => 0,
    'variable' => 7,
    'info' => 119,
  ),
  22 => 
  array (
    'mark' => 'ETL',
    'unit' => 'day',
    'exponent' => 0,
    'variable' => 7,
    'info' => 23,
  ),
  23 => 
  array (
    'mark' => 'V_info',
    'unit' => 'V',
    'exponent' => 0,
    'variable' => 7,
    'info' => 87,
  ),
  24 => 
  array (
    'mark' => '---',
    'unit' => '',
    'exponent' => 0,
    'variable' => 7,
    'info' => 23,
  ),
  25 => 
  array (
    'mark' => 'Solar batt.',
    'unit' => '',
    'exponent' => 0,
    'variable' => 10,
    'info' => 58,
  ),
  26 => 
  array (
    'mark' => 'R1',
    'unit' => '',
    'exponent' => 0,
    'variable' => 9,
    'info' => 89,
  ),
  27 => 
  array (
    'mark' => 'R2',
    'unit' => '',
    'exponent' => 0,
    'variable' => 9,
    'info' => 25,
  ),
  28 => 
  array (
    'mark' => 'R3',
    'unit' => '',
    'exponent' => 0,
    'variable' => 9,
    'info' => 25,
  ),
  29 => 
  array (
    'mark' => 'R4',
    'unit' => '',
    'exponent' => 0,
    'variable' => 9,
    'info' => 25,
  ),
  30 => 
  array (
    'mark' => 'Hi',
    'unit' => 'MJ/m3',
    'exponent' => 0,
    'variable' => 9,
    'info' => 25,
  ),
  31 => 
  array (
    'mark' => 'Zb',
    'unit' => '',
    'exponent' => 0,
    'variable' => 9,
    'info' => 25,
  ),
  32 => 
  array (
    'mark' => 'Z',
    'unit' => '',
    'exponent' => 0,
    'variable' => 9,
    'info' => 25,
  ),
  33 => 
  array (
    'mark' => 'K1',
    'unit' => '',
    'exponent' => 0,
    'variable' => 9,
    'info' => 121,
  ),
  34 => 
  array (
    'mark' => 'C',
    'unit' => '',
    'exponent' => 0,
    'variable' => 9,
    'info' => 89,
  ),
  35 => 
  array (
    'mark' => '---',
    'unit' => '',
    'exponent' => 0,
    'variable' => 9,
    'info' => 25,
  ),
  36 => 
  array (
    'mark' => '---',
    'unit' => '',
    'exponent' => 0,
    'variable' => 9,
    'info' => 25,
  ),
  37 => 
  array (
    'mark' => '---',
    'unit' => '',
    'exponent' => 0,
    'variable' => 9,
    'info' => 25,
  ),
  38 => 
  array (
    'mark' => '---',
    'unit' => '',
    'exponent' => 0,
    'variable' => 9,
    'info' => 25,
  ),
  39 => 
  array (
    'mark' => '---',
    'unit' => '',
    'exponent' => 0,
    'variable' => 7,
    'info' => 23,
  ),
  40 => 
  array (
    'mark' => 'G n',
    'unit' => '%',
    'exponent' => 0,
    'variable' => 7,
    'info' => 55,
  ),
  41 => 
  array (
    'mark' => 'W',
    'unit' => 'MJ/m3',
    'exponent' => 0,
    'variable' => 9,
    'info' => 25,
  ),
  42 => 
  array (
    'mark' => 'rom',
    'unit' => 'kg/m3',
    'exponent' => 0,
    'variable' => 9,
    'info' => 25,
  ),
  43 => 
  array (
    'mark' => 'rob',
    'unit' => 'kg/m3',
    'exponent' => 0,
    'variable' => 9,
    'info' => 25,
  ),
  44 => 
  array (
    'mark' => 'conf_algZ',
    'unit' => '',
    'exponent' => 0,
    'variable' => 4,
    'info' => 52,
  ),
  45 => 
  array (
    'mark' => 'Alarm ZSG',
    'unit' => '',
    'exponent' => 0,
    'variable' => 4,
    'info' => 52,
  ),
  46 => 
  array (
    'mark' => 'XH2',
    'unit' => '%',
    'exponent' => 0,
    'variable' => 9,
    'info' => 121,
  ),
  47 => 
  array (
    'mark' => 'XCO2',
    'unit' => '%',
    'exponent' => 0,
    'variable' => 9,
    'info' => 121,
  ),
  48 => 
  array (
    'mark' => 'XN2',
    'unit' => '%',
    'exponent' => 0,
    'variable' => 9,
    'info' => 121,
  ),
  49 => 
  array (
    'mark' => 'Hs',
    'unit' => 'MJ/m3',
    'exponent' => 0,
    'variable' => 9,
    'info' => 249,
  ),
  50 => 
  array (
    'mark' => 'd',
    'unit' => '',
    'exponent' => 0,
    'variable' => 9,
    'info' => 121,
  ),
  51 => 
  array (
    'mark' => 'C1',
    'unit' => '%',
    'exponent' => 0,
    'variable' => 9,
    'info' => 57,
  ),
  52 => 
  array (
    'mark' => 'C2',
    'unit' => '%',
    'exponent' => 0,
    'variable' => 9,
    'info' => 57,
  ),
  53 => 
  array (
    'mark' => 'C3',
    'unit' => '%',
    'exponent' => 0,
    'variable' => 9,
    'info' => 57,
  ),
  54 => 
  array (
    'mark' => 'n-C4',
    'unit' => '%',
    'exponent' => 0,
    'variable' => 9,
    'info' => 57,
  ),
  55 => 
  array (
    'mark' => 'i-C4',
    'unit' => '%',
    'exponent' => 0,
    'variable' => 9,
    'info' => 57,
  ),
  56 => 
  array (
    'mark' => 'n-C5',
    'unit' => '%',
    'exponent' => 0,
    'variable' => 9,
    'info' => 57,
  ),
  57 => 
  array (
    'mark' => 'i-C5',
    'unit' => '%',
    'exponent' => 0,
    'variable' => 9,
    'info' => 57,
  ),
  58 => 
  array (
    'mark' => 'neo-C5',
    'unit' => '%',
    'exponent' => 0,
    'variable' => 9,
    'info' => 57,
  ),
  59 => 
  array (
    'mark' => 'C6+',
    'unit' => '%',
    'exponent' => 0,
    'variable' => 9,
    'info' => 57,
  ),
  60 => 
  array (
    'mark' => 'N2',
    'unit' => '%',
    'exponent' => 0,
    'variable' => 9,
    'info' => 121,
  ),
  61 => 
  array (
    'mark' => 'CO2',
    'unit' => '%',
    'exponent' => 0,
    'variable' => 9,
    'info' => 121,
  ),
  62 => 
  array (
    'mark' => 'C6H14',
    'unit' => '%',
    'exponent' => 0,
    'variable' => 9,
    'info' => 57,
  ),
  63 => 
  array (
    'mark' => 'C7H16',
    'unit' => '%',
    'exponent' => 0,
    'variable' => 9,
    'info' => 57,
  ),
  64 => 
  array (
    'mark' => 'C8H18',
    'unit' => '%',
    'exponent' => 0,
    'variable' => 9,
    'info' => 57,
  ),
  65 => 
  array (
    'mark' => 'C9H20',
    'unit' => '%',
    'exponent' => 0,
    'variable' => 9,
    'info' => 57,
  ),
  66 => 
  array (
    'mark' => 'C10H22',
    'unit' => '%',
    'exponent' => 0,
    'variable' => 9,
    'info' => 57,
  ),
  67 => 
  array (
    'mark' => 'H2',
    'unit' => '%',
    'exponent' => 0,
    'variable' => 9,
    'info' => 121,
  ),
  68 => 
  array (
    'mark' => 'H2O',
    'unit' => '%',
    'exponent' => 0,
    'variable' => 9,
    'info' => 57,
  ),
  69 => 
  array (
    'mark' => 'H2S',
    'unit' => '%',
    'exponent' => 0,
    'variable' => 9,
    'info' => 57,
  ),
  70 => 
  array (
    'mark' => 'CO',
    'unit' => '%',
    'exponent' => 0,
    'variable' => 9,
    'info' => 57,
  ),
  71 => 
  array (
    'mark' => 'He',
    'unit' => '%',
    'exponent' => 0,
    'variable' => 9,
    'info' => 57,
  ),
  72 => 
  array (
    'mark' => 'Ar',
    'unit' => '%',
    'exponent' => 0,
    'variable' => 9,
    'info' => 57,
  ),
  73 => 
  array (
    'mark' => 'O2',
    'unit' => '%',
    'exponent' => 0,
    'variable' => 9,
    'info' => 57,
  ),
  74 => 
  array (
    'mark' => 'time m.-1',
    'unit' => '',
    'exponent' => 0,
    'variable' => 12,
    'info' => 28,
  ),
  75 => 
  array (
    'mark' => 'login-1',
    'unit' => '',
    'exponent' => 0,
    'variable' => 6,
    'info' => 22,
  ),
  76 => 
  array (
    'mark' => 'XH2-1',
    'unit' => '%',
    'exponent' => 0,
    'variable' => 9,
    'info' => 25,
  ),
  77 => 
  array (
    'mark' => 'XCO2-1',
    'unit' => '%',
    'exponent' => 0,
    'variable' => 9,
    'info' => 25,
  ),
  78 => 
  array (
    'mark' => 'XN2-1',
    'unit' => '%',
    'exponent' => 0,
    'variable' => 9,
    'info' => 25,
  ),
  79 => 
  array (
    'mark' => 'Hs-1',
    'unit' => 'MJ/m3',
    'exponent' => 0,
    'variable' => 9,
    'info' => 25,
  ),
  80 => 
  array (
    'mark' => 'd-1',
    'unit' => '',
    'exponent' => 0,
    'variable' => 9,
    'info' => 25,
  ),
  81 => 
  array (
    'mark' => 'C1-1',
    'unit' => '%',
    'exponent' => 0,
    'variable' => 9,
    'info' => 25,
  ),
  82 => 
  array (
    'mark' => 'C2-1',
    'unit' => '%',
    'exponent' => 0,
    'variable' => 9,
    'info' => 25,
  ),
  83 => 
  array (
    'mark' => 'C3-1',
    'unit' => '%',
    'exponent' => 0,
    'variable' => 9,
    'info' => 25,
  ),
  84 => 
  array (
    'mark' => 'n-C4-1',
    'unit' => '%',
    'exponent' => 0,
    'variable' => 9,
    'info' => 25,
  ),
  85 => 
  array (
    'mark' => 'i-C4-1',
    'unit' => '%',
    'exponent' => 0,
    'variable' => 9,
    'info' => 25,
  ),
  86 => 
  array (
    'mark' => 'n-C5-1',
    'unit' => '%',
    'exponent' => 0,
    'variable' => 9,
    'info' => 25,
  ),
  87 => 
  array (
    'mark' => 'i-C5-1',
    'unit' => '%',
    'exponent' => 0,
    'variable' => 9,
    'info' => 25,
  ),
  88 => 
  array (
    'mark' => 'neo-C5-1',
    'unit' => '%',
    'exponent' => 0,
    'variable' => 9,
    'info' => 25,
  ),
  89 => 
  array (
    'mark' => 'C6+-1',
    'unit' => '%',
    'exponent' => 0,
    'variable' => 9,
    'info' => 25,
  ),
  90 => 
  array (
    'mark' => 'N2-1',
    'unit' => '%',
    'exponent' => 0,
    'variable' => 9,
    'info' => 25,
  ),
  91 => 
  array (
    'mark' => 'CO2-1',
    'unit' => '%',
    'exponent' => 0,
    'variable' => 9,
    'info' => 25,
  ),
  92 => 
  array (
    'mark' => 'C6H14-1',
    'unit' => '%',
    'exponent' => 0,
    'variable' => 9,
    'info' => 25,
  ),
  93 => 
  array (
    'mark' => 'C7H16-1',
    'unit' => '%',
    'exponent' => 0,
    'variable' => 9,
    'info' => 25,
  ),
  94 => 
  array (
    'mark' => 'C8H18-1',
    'unit' => '%',
    'exponent' => 0,
    'variable' => 9,
    'info' => 25,
  ),
  95 => 
  array (
    'mark' => 'C9H20-1',
    'unit' => '%',
    'exponent' => 0,
    'variable' => 9,
    'info' => 25,
  ),
  96 => 
  array (
    'mark' => 'C10H22-1',
    'unit' => '%',
    'exponent' => 0,
    'variable' => 9,
    'info' => 25,
  ),
  97 => 
  array (
    'mark' => 'H2-1',
    'unit' => '%',
    'exponent' => 0,
    'variable' => 9,
    'info' => 25,
  ),
  98 => 
  array (
    'mark' => 'H2O-1',
    'unit' => '%',
    'exponent' => 0,
    'variable' => 9,
    'info' => 25,
  ),
  99 => 
  array (
    'mark' => 'H2S-1',
    'unit' => '%',
    'exponent' => 0,
    'variable' => 9,
    'info' => 25,
  ),
  100 => 
  array (
    'mark' => 'CO-1',
    'unit' => '%',
    'exponent' => 0,
    'variable' => 9,
    'info' => 25,
  ),
  101 => 
  array (
    'mark' => 'He-1',
    'unit' => '%',
    'exponent' => 0,
    'variable' => 9,
    'info' => 25,
  ),
  102 => 
  array (
    'mark' => 'Ar-1',
    'unit' => '%',
    'exponent' => 0,
    'variable' => 9,
    'info' => 25,
  ),
  103 => 
  array (
    'mark' => 'O2-1',
    'unit' => '%',
    'exponent' => 0,
    'variable' => 9,
    'info' => 25,
  ),
  104 => 
  array (
    'mark' => '---',
    'unit' => '',
    'exponent' => 0,
    'variable' => 7,
    'info' => 23,
  ),
  105 => 
  array (
    'mark' => '---',
    'unit' => '',
    'exponent' => 0,
    'variable' => 7,
    'info' => 23,
  ),
  106 => 
  array (
    'mark' => '---',
    'unit' => '',
    'exponent' => 0,
    'variable' => 7,
    'info' => 23,
  ),
  107 => 
  array (
    'mark' => '---',
    'unit' => '',
    'exponent' => 0,
    'variable' => 7,
    'info' => 23,
  ),
  108 => 
  array (
    'mark' => '---',
    'unit' => '',
    'exponent' => 0,
    'variable' => 7,
    'info' => 23,
  ),
  109 => 
  array (
    'mark' => '---',
    'unit' => '',
    'exponent' => 0,
    'variable' => 7,
    'info' => 23,
  ),
  110 => 
  array (
    'mark' => '---',
    'unit' => '',
    'exponent' => 0,
    'variable' => 7,
    'info' => 23,
  ),
  111 => 
  array (
    'mark' => '---',
    'unit' => '',
    'exponent' => 0,
    'variable' => 7,
    'info' => 23,
  ),
  112 => 
  array (
    'mark' => '---',
    'unit' => '',
    'exponent' => 0,
    'variable' => 7,
    'info' => 23,
  ),
  113 => 
  array (
    'mark' => '---',
    'unit' => '',
    'exponent' => 0,
    'variable' => 7,
    'info' => 23,
  ),
  114 => 
  array (
    'mark' => '---',
    'unit' => '',
    'exponent' => 0,
    'variable' => 7,
    'info' => 23,
  ),
  115 => 
  array (
    'mark' => '---',
    'unit' => '',
    'exponent' => 0,
    'variable' => 7,
    'info' => 23,
  ),
  116 => 
  array (
    'mark' => '---',
    'unit' => '',
    'exponent' => 0,
    'variable' => 7,
    'info' => 23,
  ),
  117 => 
  array (
    'mark' => '---',
    'unit' => '',
    'exponent' => 0,
    'variable' => 7,
    'info' => 23,
  ),
  118 => 
  array (
    'mark' => '---',
    'unit' => '',
    'exponent' => 0,
    'variable' => 7,
    'info' => 23,
  ),
  119 => 
  array (
    'mark' => '---',
    'unit' => '',
    'exponent' => 0,
    'variable' => 7,
    'info' => 23,
  ),
  120 => 
  array (
    'mark' => 'T1',
    'unit' => 'K',
    'exponent' => 0,
    'variable' => 9,
    'info' => 57,
  ),
  121 => 
  array (
    'mark' => 'LF factor',
    'unit' => 'm3/imp',
    'exponent' => 0,
    'variable' => 9,
    'info' => 57,
  ),
  122 => 
  array (
    'mark' => 'HF factor',
    'unit' => 'imp/m3',
    'exponent' => 0,
    'variable' => 9,
    'info' => 57,
  ),
  123 => 
  array (
    'mark' => 'HF/LF',
    'unit' => '',
    'exponent' => 0,
    'variable' => 7,
    'info' => 23,
  ),
  124 => 
  array (
    'mark' => 'LF time',
    'unit' => 's',
    'exponent' => 0,
    'variable' => 9,
    'info' => 25,
  ),
  125 => 
  array (
    'mark' => 'pb',
    'unit' => 'bar',
    'exponent' => 0,
    'variable' => 9,
    'info' => 57,
  ),
  126 => 
  array (
    'mark' => 'Tb',
    'unit' => 'K',
    'exponent' => 0,
    'variable' => 9,
    'info' => 57,
  ),
  127 => 
  array (
    'mark' => 'Index A',
    'unit' => '',
    'exponent' => 0,
    'variable' => 6,
    'info' => 22,
  ),
  128 => 
  array (
    'mark' => 'Index Am',
    'unit' => '',
    'exponent' => 0,
    'variable' => 6,
    'info' => 22,
  ),
  129 => 
  array (
    'mark' => 'Alarm 1',
    'unit' => '',
    'exponent' => 0,
    'variable' => 6,
    'info' => 22,
  ),
  130 => 
  array (
    'mark' => 'Alarm 2',
    'unit' => '',
    'exponent' => 0,
    'variable' => 6,
    'info' => 22,
  ),
  131 => 
  array (
    'mark' => 'Account',
    'unit' => '',
    'exponent' => 0,
    'variable' => 6,
    'info' => 22,
  ),
  132 => 
  array (
    'mark' => 'Customer',
    'unit' => '',
    'exponent' => 0,
    'variable' => 12,
    'info' => 60,
  ),
  133 => 
  array (
    'mark' => 'last idx',
    'unit' => '',
    'exponent' => 0,
    'variable' => 5,
    'info' => 21,
  ),
  134 => 
  array (
    'mark' => 'last v1',
    'unit' => '',
    'exponent' => 0,
    'variable' => 9,
    'info' => 25,
  ),
  135 => 
  array (
    'mark' => 'last v2',
    'unit' => '',
    'exponent' => 0,
    'variable' => 9,
    'info' => 25,
  ),
  136 => 
  array (
    'mark' => 'auto DST',
    'unit' => '',
    'exponent' => 0,
    'variable' => 10,
    'info' => 58,
  ),
  137 => 
  array (
    'mark' => 'lock CFG',
    'unit' => '',
    'exponent' => 0,
    'variable' => 10,
    'info' => 26,
  ),
  138 => 
  array (
    'mark' => 'dt/tm',
    'unit' => '',
    'exponent' => 0,
    'variable' => 12,
    'info' => 28,
  ),
  139 => 
  array (
    'mark' => 'dt_ymdw',
    'unit' => '',
    'exponent' => 0,
    'variable' => 6,
    'info' => 22,
  ),
  140 => 
  array (
    'mark' => 'tm_hmss',
    'unit' => '',
    'exponent' => 0,
    'variable' => 6,
    'info' => 22,
  ),
  141 => 
  array (
    'mark' => 'Device s/n',
    'unit' => '',
    'exponent' => 0,
    'variable' => 12,
    'info' => 60,
  ),
  142 => 
  array (
    'mark' => 's',
    'unit' => '',
    'exponent' => 0,
    'variable' => 12,
    'info' => 60,
  ),
  143 => 
  array (
    'mark' => 'SV',
    'unit' => '',
    'exponent' => 0,
    'variable' => 12,
    'info' => 28,
  ),
  144 => 
  array (
    'mark' => 'HV',
    'unit' => '',
    'exponent' => 0,
    'variable' => 12,
    'info' => 28,
  ),
  145 => 
  array (
    'mark' => 'vDP',
    'unit' => '',
    'exponent' => 0,
    'variable' => 12,
    'info' => 28,
  ),
  146 => 
  array (
    'mark' => 'vZD',
    'unit' => '',
    'exponent' => 0,
    'variable' => 12,
    'info' => 28,
  ),
  147 => 
  array (
    'mark' => 'contrast',
    'unit' => '',
    'exponent' => 0,
    'variable' => 6,
    'info' => 54,
  ),
  148 => 
  array (
    'mark' => 'bright',
    'unit' => '',
    'exponent' => 0,
    'variable' => 6,
    'info' => 54,
  ),
  149 => 
  array (
    'mark' => 'backlight',
    'unit' => 's',
    'exponent' => 0,
    'variable' => 5,
    'info' => 53,
  ),
  150 => 
  array (
    'mark' => 'Device name',
    'unit' => '',
    'exponent' => 0,
    'variable' => 12,
    'info' => 28,
  ),
  151 => 
  array (
    'mark' => 'dtau',
    'unit' => 'min',
    'exponent' => 0,
    'variable' => 5,
    'info' => 53,
  ),
  152 => 
  array (
    'mark' => 'max reg',
    'unit' => 'day',
    'exponent' => 0,
    'variable' => 5,
    'info' => 21,
  ),
  153 => 
  array (
    'mark' => 'Billing hour',
    'unit' => 'h',
    'exponent' => 0,
    'variable' => 4,
    'info' => 52,
  ),
  154 => 
  array (
    'mark' => 'Billing day',
    'unit' => 'day',
    'exponent' => 0,
    'variable' => 4,
    'info' => 52,
  ),
  155 => 
  array (
    'mark' => 'conf_imp',
    'unit' => '',
    'exponent' => 0,
    'variable' => 4,
    'info' => 52,
  ),
  156 => 
  array (
    'mark' => 'conf_subst',
    'unit' => '',
    'exponent' => 0,
    'variable' => 6,
    'info' => 54,
  ),
  157 => 
  array (
    'mark' => 'p1 subst',
    'unit' => 'kPa',
    'exponent' => 0,
    'variable' => 9,
    'info' => 57,
  ),
  158 => 
  array (
    'mark' => 't subst',
    'unit' => '°C',
    'exponent' => 0,
    'variable' => 9,
    'info' => 57,
  ),
  159 => 
  array (
    'mark' => 'p2 subst',
    'unit' => 'kPa',
    'exponent' => 0,
    'variable' => 9,
    'info' => 57,
  ),
  160 => 
  array (
    'mark' => 'Qb peak max',
    'unit' => 'm3/h',
    'exponent' => 0,
    'variable' => 7,
    'info' => 87,
  ),
  161 => 
  array (
    'mark' => 'p1 peak min',
    'unit' => 'kPa',
    'exponent' => 0,
    'variable' => 9,
    'info' => 89,
  ),
  162 => 
  array (
    'mark' => 'p1 peak max',
    'unit' => 'kPa',
    'exponent' => 0,
    'variable' => 9,
    'info' => 25,
  ),
  163 => 
  array (
    'mark' => 'p2 peak min',
    'unit' => 'kPa',
    'exponent' => 0,
    'variable' => 9,
    'info' => 25,
  ),
  164 => 
  array (
    'mark' => 'p2 peak max',
    'unit' => 'kPa',
    'exponent' => 0,
    'variable' => 9,
    'info' => 25,
  ),
  165 => 
  array (
    'mark' => 'Vb0',
    'unit' => 'm3',
    'exponent' => 0,
    'variable' => 7,
    'info' => 23,
  ),
  166 => 
  array (
    'mark' => 'Vb1',
    'unit' => 'm3',
    'exponent' => 4,
    'variable' => 7,
    'info' => 23,
  ),
  167 => 
  array (
    'mark' => 'Vm0',
    'unit' => 'm3',
    'exponent' => 0,
    'variable' => 7,
    'info' => 23,
  ),
  168 => 
  array (
    'mark' => 'Vm1',
    'unit' => 'm3',
    'exponent' => 4,
    'variable' => 7,
    'info' => 23,
  ),
  169 => 
  array (
    'mark' => 'E0',
    'unit' => 'kWh',
    'exponent' => 0,
    'variable' => 7,
    'info' => 23,
  ),
  170 => 
  array (
    'mark' => 'E1',
    'unit' => 'kWh',
    'exponent' => 4,
    'variable' => 7,
    'info' => 23,
  ),
  171 => 
  array (
    'mark' => 'qb',
    'unit' => 'm3/h',
    'exponent' => 0,
    'variable' => 9,
    'info' => 25,
  ),
  172 => 
  array (
    'mark' => 'qm',
    'unit' => 'm3/h',
    'exponent' => 0,
    'variable' => 9,
    'info' => 25,
  ),
  173 => 
  array (
    'mark' => 'S',
    'unit' => '',
    'exponent' => 0,
    'variable' => 4,
    'info' => 52,
  ),
  174 => 
  array (
    'mark' => 'S act',
    'unit' => '',
    'exponent' => 0,
    'variable' => 4,
    'info' => 20,
  ),
  175 => 
  array (
    'mark' => 'S pol',
    'unit' => '',
    'exponent' => 0,
    'variable' => 4,
    'info' => 52,
  ),
  176 => 
  array (
    'mark' => '---',
    'unit' => '',
    'exponent' => 0,
    'variable' => 12,
    'info' => 28,
  ),
  177 => 
  array (
    'mark' => '---',
    'unit' => '',
    'exponent' => 0,
    'variable' => 12,
    'info' => 28,
  ),
  178 => 
  array (
    'mark' => '---',
    'unit' => '',
    'exponent' => 0,
    'variable' => 12,
    'info' => 28,
  ),
  179 => 
  array (
    'mark' => 'Index R',
    'unit' => '',
    'exponent' => 0,
    'variable' => 6,
    'info' => 22,
  ),
  180 => 
  array (
    'mark' => 'Qm min',
    'unit' => 'm3/h',
    'exponent' => 0,
    'variable' => 7,
    'info' => 55,
  ),
  181 => 
  array (
    'mark' => 'Qm max',
    'unit' => 'm3/h',
    'exponent' => 0,
    'variable' => 7,
    'info' => 55,
  ),
  182 => 
  array (
    'mark' => 'p1 min',
    'unit' => 'kPa',
    'exponent' => 0,
    'variable' => 9,
    'info' => 57,
  ),
  183 => 
  array (
    'mark' => 'p1 max',
    'unit' => 'kPa',
    'exponent' => 0,
    'variable' => 9,
    'info' => 57,
  ),
  184 => 
  array (
    'mark' => 'p2 min',
    'unit' => 'kPa',
    'exponent' => 0,
    'variable' => 9,
    'info' => 57,
  ),
  185 => 
  array (
    'mark' => 'p2 max',
    'unit' => 'kPa',
    'exponent' => 0,
    'variable' => 9,
    'info' => 57,
  ),
  186 => 
  array (
    'mark' => 't min',
    'unit' => '°C',
    'exponent' => 0,
    'variable' => 9,
    'info' => 57,
  ),
  187 => 
  array (
    'mark' => 't max',
    'unit' => '°C',
    'exponent' => 0,
    'variable' => 9,
    'info' => 57,
  ),
  188 => 
  array (
    'mark' => 'tamb min',
    'unit' => '°C',
    'exponent' => 0,
    'variable' => 9,
    'info' => 25,
  ),
  189 => 
  array (
    'mark' => 'tamb max',
    'unit' => '°C',
    'exponent' => 0,
    'variable' => 9,
    'info' => 25,
  ),
  190 => 
  array (
    'mark' => 'l dVbh 1',
    'unit' => 'm3',
    'exponent' => 0,
    'variable' => 9,
    'info' => 57,
  ),
  191 => 
  array (
    'mark' => 'l dVbh 2',
    'unit' => 'm3',
    'exponent' => 0,
    'variable' => 9,
    'info' => 57,
  ),
  192 => 
  array (
    'mark' => 'l dVbh 3',
    'unit' => 'm3',
    'exponent' => 0,
    'variable' => 9,
    'info' => 57,
  ),
  193 => 
  array (
    'mark' => 'l Qb',
    'unit' => 'm3/h',
    'exponent' => 0,
    'variable' => 7,
    'info' => 55,
  ),
  194 => 
  array (
    'mark' => 'u Qb',
    'unit' => 'm3/h',
    'exponent' => 0,
    'variable' => 7,
    'info' => 55,
  ),
  195 => 
  array (
    'mark' => 'l Qm',
    'unit' => 'm3/h',
    'exponent' => 0,
    'variable' => 7,
    'info' => 55,
  ),
  196 => 
  array (
    'mark' => 'u Qm',
    'unit' => 'm3/h',
    'exponent' => 0,
    'variable' => 7,
    'info' => 55,
  ),
  197 => 
  array (
    'mark' => 'Vm-V2',
    'unit' => 'm3',
    'exponent' => 0,
    'variable' => 7,
    'info' => 55,
  ),
  198 => 
  array (
    'mark' => 'l HF/LF',
    'unit' => '',
    'exponent' => 0,
    'variable' => 9,
    'info' => 57,
  ),
  199 => 
  array (
    'mark' => 't HF/LF',
    'unit' => '%',
    'exponent' => 0,
    'variable' => 9,
    'info' => 57,
  ),
  200 => 
  array (
    'mark' => 'l p1',
    'unit' => 'kPa',
    'exponent' => 0,
    'variable' => 9,
    'info' => 57,
  ),
  201 => 
  array (
    'mark' => 'u p1',
    'unit' => 'kPa',
    'exponent' => 0,
    'variable' => 9,
    'info' => 57,
  ),
  202 => 
  array (
    'mark' => 'l p2',
    'unit' => 'kPa',
    'exponent' => 0,
    'variable' => 9,
    'info' => 57,
  ),
  203 => 
  array (
    'mark' => 'u p2',
    'unit' => 'kPa',
    'exponent' => 0,
    'variable' => 9,
    'info' => 57,
  ),
  204 => 
  array (
    'mark' => 'l t',
    'unit' => '°C',
    'exponent' => 0,
    'variable' => 9,
    'info' => 57,
  ),
  205 => 
  array (
    'mark' => 'u t',
    'unit' => '°C',
    'exponent' => 0,
    'variable' => 9,
    'info' => 57,
  ),
  206 => 
  array (
    'mark' => 'l R1',
    'unit' => '',
    'exponent' => 0,
    'variable' => 9,
    'info' => 57,
  ),
  207 => 
  array (
    'mark' => 'u R1',
    'unit' => '',
    'exponent' => 0,
    'variable' => 9,
    'info' => 57,
  ),
  208 => 
  array (
    'mark' => 'l R2',
    'unit' => '',
    'exponent' => 0,
    'variable' => 9,
    'info' => 57,
  ),
  209 => 
  array (
    'mark' => 'u R2',
    'unit' => '',
    'exponent' => 0,
    'variable' => 9,
    'info' => 57,
  ),
  210 => 
  array (
    'mark' => 'l R3',
    'unit' => '',
    'exponent' => 0,
    'variable' => 9,
    'info' => 57,
  ),
  211 => 
  array (
    'mark' => 'u R3',
    'unit' => '',
    'exponent' => 0,
    'variable' => 9,
    'info' => 57,
  ),
  212 => 
  array (
    'mark' => 'l R4',
    'unit' => '',
    'exponent' => 0,
    'variable' => 9,
    'info' => 57,
  ),
  213 => 
  array (
    'mark' => 'u R4',
    'unit' => '',
    'exponent' => 0,
    'variable' => 9,
    'info' => 57,
  ),
  214 => 
  array (
    'mark' => 'meter s/n',
    'unit' => '',
    'exponent' => 0,
    'variable' => 12,
    'info' => 60,
  ),
  215 => 
  array (
    'mark' => 'p1 s/n',
    'unit' => '',
    'exponent' => 0,
    'variable' => 12,
    'info' => 60,
  ),
  216 => 
  array (
    'mark' => 't s/n',
    'unit' => '',
    'exponent' => 0,
    'variable' => 12,
    'info' => 60,
  ),
  217 => 
  array (
    'mark' => 'R1 s/n',
    'unit' => '',
    'exponent' => 0,
    'variable' => 12,
    'info' => 60,
  ),
  218 => 
  array (
    'mark' => 'R2 s/n',
    'unit' => '',
    'exponent' => 0,
    'variable' => 12,
    'info' => 60,
  ),
  219 => 
  array (
    'mark' => 'R3 s/n',
    'unit' => '',
    'exponent' => 0,
    'variable' => 12,
    'info' => 60,
  ),
  220 => 
  array (
    'mark' => 'R4 s/n',
    'unit' => '',
    'exponent' => 0,
    'variable' => 12,
    'info' => 60,
  ),
  221 => 
  array (
    'mark' => 'R1 address',
    'unit' => '',
    'exponent' => 0,
    'variable' => 5,
    'info' => 53,
  ),
  222 => 
  array (
    'mark' => 'R2 address',
    'unit' => '',
    'exponent' => 0,
    'variable' => 5,
    'info' => 53,
  ),
  223 => 
  array (
    'mark' => 'R3 address',
    'unit' => '',
    'exponent' => 0,
    'variable' => 5,
    'info' => 53,
  ),
  224 => 
  array (
    'mark' => 'R4 address',
    'unit' => '',
    'exponent' => 0,
    'variable' => 5,
    'info' => 53,
  ),
  225 => 
  array (
    'mark' => 'R1 idx',
    'unit' => '',
    'exponent' => 0,
    'variable' => 5,
    'info' => 53,
  ),
  226 => 
  array (
    'mark' => 'R2 idx',
    'unit' => '',
    'exponent' => 0,
    'variable' => 5,
    'info' => 53,
  ),
  227 => 
  array (
    'mark' => 'R3 idx',
    'unit' => '',
    'exponent' => 0,
    'variable' => 5,
    'info' => 53,
  ),
  228 => 
  array (
    'mark' => 'R4 idx',
    'unit' => '',
    'exponent' => 0,
    'variable' => 5,
    'info' => 53,
  ),
  229 => 
  array (
    'mark' => 'R1 type',
    'unit' => '',
    'exponent' => 0,
    'variable' => 4,
    'info' => 52,
  ),
  230 => 
  array (
    'mark' => 'R2 type',
    'unit' => '',
    'exponent' => 0,
    'variable' => 4,
    'info' => 52,
  ),
  231 => 
  array (
    'mark' => 'R3 type',
    'unit' => '',
    'exponent' => 0,
    'variable' => 4,
    'info' => 52,
  ),
  232 => 
  array (
    'mark' => 'R4 type',
    'unit' => '',
    'exponent' => 0,
    'variable' => 4,
    'info' => 52,
  ),
  233 => 
  array (
    'mark' => 'LF',
    'unit' => 'imp',
    'exponent' => 0,
    'variable' => 6,
    'info' => 22,
  ),
  234 => 
  array (
    'mark' => 'HF',
    'unit' => 'imp',
    'exponent' => 0,
    'variable' => 6,
    'info' => 22,
  ),
  235 => 
  array (
    'mark' => 'p1_bin',
    'unit' => 'bin',
    'exponent' => 0,
    'variable' => 5,
    'info' => 21,
  ),
  236 => 
  array (
    'mark' => 'p2_bin',
    'unit' => 'bin',
    'exponent' => 0,
    'variable' => 5,
    'info' => 21,
  ),
  237 => 
  array (
    'mark' => 't_bin',
    'unit' => 'bin',
    'exponent' => 0,
    'variable' => 5,
    'info' => 21,
  ),
  238 => 
  array (
    'mark' => 'tamb_bin',
    'unit' => 'bin',
    'exponent' => 0,
    'variable' => 5,
    'info' => 21,
  ),
  239 => 
  array (
    'mark' => 'OC',
    'unit' => '',
    'exponent' => 0,
    'variable' => 4,
    'info' => 52,
  ),
  240 => 
  array (
    'mark' => 'OCF',
    'unit' => '',
    'exponent' => 0,
    'variable' => 4,
    'info' => 52,
  ),
  241 => 
  array (
    'mark' => 'OCF_idx',
    'unit' => '',
    'exponent' => 0,
    'variable' => 5,
    'info' => 53,
  ),
  242 => 
  array (
    'mark' => 'OCF_min',
    'unit' => '',
    'exponent' => 0,
    'variable' => 9,
    'info' => 57,
  ),
  243 => 
  array (
    'mark' => 'OCF_max',
    'unit' => '',
    'exponent' => 0,
    'variable' => 9,
    'info' => 57,
  ),
  244 => 
  array (
    'mark' => 'F_min',
    'unit' => 'Hz',
    'exponent' => 0,
    'variable' => 6,
    'info' => 54,
  ),
  245 => 
  array (
    'mark' => 'F_max',
    'unit' => 'Hz',
    'exponent' => 0,
    'variable' => 6,
    'info' => 54,
  ),
  246 => 
  array (
    'mark' => 'F_out',
    'unit' => 'Hz',
    'exponent' => 0,
    'variable' => 7,
    'info' => 23,
  ),
  247 => 
  array (
    'mark' => 'OC act',
    'unit' => '',
    'exponent' => 0,
    'variable' => 4,
    'info' => 20,
  ),
  248 => 
  array (
    'mark' => 'OC pol',
    'unit' => '',
    'exponent' => 0,
    'variable' => 4,
    'info' => 52,
  ),
  249 => 
  array (
    'mark' => 'OC1 idx',
    'unit' => '',
    'exponent' => 0,
    'variable' => 5,
    'info' => 53,
  ),
  250 => 
  array (
    'mark' => 'OC2 idx',
    'unit' => '',
    'exponent' => 0,
    'variable' => 5,
    'info' => 53,
  ),
  251 => 
  array (
    'mark' => '---',
    'unit' => '',
    'exponent' => 0,
    'variable' => 6,
    'info' => 22,
  ),
  252 => 
  array (
    'mark' => 'NOC1',
    'unit' => '',
    'exponent' => 0,
    'variable' => 12,
    'info' => 60,
  ),
  253 => 
  array (
    'mark' => 'NOC2',
    'unit' => '',
    'exponent' => 0,
    'variable' => 12,
    'info' => 60,
  ),
  254 => 
  array (
    'mark' => '---',
    'unit' => '',
    'exponent' => 0,
    'variable' => 6,
    'info' => 22,
  ),
  255 => 
  array (
    'mark' => 'Vbe',
    'unit' => 'm3',
    'exponent' => 0,
    'variable' => 9,
    'info' => 217,
  ),
  256 => 
  array (
    'mark' => 'Ee',
    'unit' => 'kWh',
    'exponent' => 0,
    'variable' => 9,
    'info' => 217,
  ),
  257 => 
  array (
    'mark' => 'Me',
    'unit' => 'kg',
    'exponent' => 0,
    'variable' => 9,
    'info' => 25,
  ),
  258 => 
  array (
    'mark' => 'HFmaintainT',
    'unit' => 's',
    'exponent' => 0,
    'variable' => 6,
    'info' => 54,
  ),
  259 => 
  array (
    'mark' => 'eph Vb',
    'unit' => 'm3',
    'exponent' => 0,
    'variable' => 9,
    'info' => 25,
  ),
  260 => 
  array (
    'mark' => 'ph Vb',
    'unit' => 'm3',
    'exponent' => 0,
    'variable' => 9,
    'info' => 153,
  ),
  261 => 
  array (
    'mark' => 'ph Vb time',
    'unit' => '',
    'exponent' => 0,
    'variable' => 6,
    'info' => 150,
  ),
  262 => 
  array (
    'mark' => 'dVbh',
    'unit' => 'm3',
    'exponent' => 0,
    'variable' => 9,
    'info' => 25,
  ),
  263 => 
  array (
    'mark' => 'eph E',
    'unit' => 'kWh',
    'exponent' => 0,
    'variable' => 9,
    'info' => 25,
  ),
  264 => 
  array (
    'mark' => 'ph E',
    'unit' => 'kWh',
    'exponent' => 0,
    'variable' => 9,
    'info' => 153,
  ),
  265 => 
  array (
    'mark' => 'ph E time',
    'unit' => '',
    'exponent' => 0,
    'variable' => 6,
    'info' => 150,
  ),
  266 => 
  array (
    'mark' => 'dEh',
    'unit' => 'kWh',
    'exponent' => 0,
    'variable' => 9,
    'info' => 25,
  ),
  267 => 
  array (
    'mark' => 'p1h',
    'unit' => 'kPa',
    'exponent' => 0,
    'variable' => 9,
    'info' => 153,
  ),
  268 => 
  array (
    'mark' => 'th',
    'unit' => '°C',
    'exponent' => 0,
    'variable' => 9,
    'info' => 153,
  ),
  269 => 
  array (
    'mark' => 'dVbD last',
    'unit' => 'm3',
    'exponent' => 0,
    'variable' => 9,
    'info' => 25,
  ),
  270 => 
  array (
    'mark' => 'dVbM last',
    'unit' => 'm3',
    'exponent' => 0,
    'variable' => 9,
    'info' => 25,
  ),
  271 => 
  array (
    'mark' => 'dED last',
    'unit' => 'kWh',
    'exponent' => 0,
    'variable' => 9,
    'info' => 25,
  ),
  272 => 
  array (
    'mark' => 'dEM last',
    'unit' => 'kWh',
    'exponent' => 0,
    'variable' => 9,
    'info' => 25,
  ),
  273 => 
  array (
    'mark' => 'ph Vb C',
    'unit' => 'm3',
    'exponent' => 0,
    'variable' => 9,
    'info' => 25,
  ),
  274 => 
  array (
    'mark' => 'ph Vb P',
    'unit' => 'm3',
    'exponent' => 0,
    'variable' => 9,
    'info' => 25,
  ),
  275 => 
  array (
    'mark' => 'ph E C',
    'unit' => 'kWh',
    'exponent' => 0,
    'variable' => 9,
    'info' => 25,
  ),
  276 => 
  array (
    'mark' => 'ph E P',
    'unit' => 'kWh',
    'exponent' => 0,
    'variable' => 9,
    'info' => 25,
  ),
  277 => 
  array (
    'mark' => 'p1 min D',
    'unit' => 'kPa',
    'exponent' => 0,
    'variable' => 9,
    'info' => 153,
  ),
  278 => 
  array (
    'mark' => 'p1 max D',
    'unit' => 'kPa',
    'exponent' => 0,
    'variable' => 9,
    'info' => 153,
  ),
  279 => 
  array (
    'mark' => 't min D',
    'unit' => '°C',
    'exponent' => 0,
    'variable' => 9,
    'info' => 153,
  ),
  280 => 
  array (
    'mark' => 't max D',
    'unit' => '°C',
    'exponent' => 0,
    'variable' => 9,
    'info' => 153,
  ),
  281 => 
  array (
    'mark' => 'Qb min D',
    'unit' => 'm3/h',
    'exponent' => 0,
    'variable' => 9,
    'info' => 153,
  ),
  282 => 
  array (
    'mark' => 'Qb max D',
    'unit' => 'm3/h',
    'exponent' => 0,
    'variable' => 9,
    'info' => 153,
  ),
  283 => 
  array (
    'mark' => 'FlowTime',
    'unit' => 's',
    'exponent' => 0,
    'variable' => 6,
    'info' => 214,
  ),
  284 => 
  array (
    'mark' => '---',
    'unit' => '',
    'exponent' => 0,
    'variable' => 9,
    'info' => 25,
  ),
  285 => 
  array (
    'mark' => '---',
    'unit' => '',
    'exponent' => 0,
    'variable' => 9,
    'info' => 25,
  ),
  286 => 
  array (
    'mark' => '---',
    'unit' => '',
    'exponent' => 0,
    'variable' => 9,
    'info' => 25,
  ),
  287 => 
  array (
    'mark' => '---',
    'unit' => '',
    'exponent' => 0,
    'variable' => 9,
    'info' => 25,
  ),
  288 => 
  array (
    'mark' => '---',
    'unit' => '',
    'exponent' => 0,
    'variable' => 9,
    'info' => 25,
  ),
  289 => 
  array (
    'mark' => 'Vbek',
    'unit' => 'm3',
    'exponent' => 0,
    'variable' => 9,
    'info' => 25,
  ),
  290 => 
  array (
    'mark' => 'Eek',
    'unit' => 'kWh',
    'exponent' => 0,
    'variable' => 9,
    'info' => 25,
  ),
  291 => 
  array (
    'mark' => 'Mek',
    'unit' => 'kg',
    'exponent' => 0,
    'variable' => 9,
    'info' => 25,
  ),
  292 => 
  array (
    'mark' => 'Vbk',
    'unit' => 'm3',
    'exponent' => 0,
    'variable' => 9,
    'info' => 25,
  ),
  293 => 
  array (
    'mark' => 'Vmk',
    'unit' => 'm3',
    'exponent' => 0,
    'variable' => 9,
    'info' => 25,
  ),
  294 => 
  array (
    'mark' => 'Ek',
    'unit' => 'kWh',
    'exponent' => 0,
    'variable' => 9,
    'info' => 25,
  ),
  295 => 
  array (
    'mark' => 'Mk',
    'unit' => 'kg',
    'exponent' => 0,
    'variable' => 9,
    'info' => 25,
  ),
  296 => 
  array (
    'mark' => 'error_curve_corr',
    'unit' => '',
    'exponent' => 0,
    'variable' => 4,
    'info' => 52,
  ),
  297 => 
  array (
    'mark' => 'fP1',
    'unit' => '%',
    'exponent' => 0,
    'variable' => 9,
    'info' => 57,
  ),
  298 => 
  array (
    'mark' => 'fP2',
    'unit' => '%',
    'exponent' => 0,
    'variable' => 9,
    'info' => 57,
  ),
  299 => 
  array (
    'mark' => 'fP3',
    'unit' => '%',
    'exponent' => 0,
    'variable' => 9,
    'info' => 57,
  ),
  300 => 
  array (
    'mark' => 'fP4',
    'unit' => '%',
    'exponent' => 0,
    'variable' => 9,
    'info' => 57,
  ),
  301 => 
  array (
    'mark' => 'fP5',
    'unit' => '%',
    'exponent' => 0,
    'variable' => 9,
    'info' => 57,
  ),
  302 => 
  array (
    'mark' => 'fP6',
    'unit' => '%',
    'exponent' => 0,
    'variable' => 9,
    'info' => 57,
  ),
  303 => 
  array (
    'mark' => 'fP7',
    'unit' => '%',
    'exponent' => 0,
    'variable' => 9,
    'info' => 57,
  ),
  304 => 
  array (
    'mark' => 'fP8',
    'unit' => '%',
    'exponent' => 0,
    'variable' => 9,
    'info' => 57,
  ),
  305 => 
  array (
    'mark' => 'fP9',
    'unit' => '%',
    'exponent' => 0,
    'variable' => 9,
    'info' => 57,
  ),
  306 => 
  array (
    'mark' => 'fP10',
    'unit' => '%',
    'exponent' => 0,
    'variable' => 9,
    'info' => 57,
  ),
  307 => 
  array (
    'mark' => 'QP1',
    'unit' => 'm3/h',
    'exponent' => 0,
    'variable' => 9,
    'info' => 57,
  ),
  308 => 
  array (
    'mark' => 'QP2',
    'unit' => 'm3/h',
    'exponent' => 0,
    'variable' => 9,
    'info' => 57,
  ),
  309 => 
  array (
    'mark' => 'QP3',
    'unit' => 'm3/h',
    'exponent' => 0,
    'variable' => 9,
    'info' => 57,
  ),
  310 => 
  array (
    'mark' => 'QP4',
    'unit' => 'm3/h',
    'exponent' => 0,
    'variable' => 9,
    'info' => 57,
  ),
  311 => 
  array (
    'mark' => 'QP5',
    'unit' => 'm3/h',
    'exponent' => 0,
    'variable' => 9,
    'info' => 57,
  ),
  312 => 
  array (
    'mark' => 'QP6',
    'unit' => 'm3/h',
    'exponent' => 0,
    'variable' => 9,
    'info' => 57,
  ),
  313 => 
  array (
    'mark' => 'QP7',
    'unit' => 'm3/h',
    'exponent' => 0,
    'variable' => 9,
    'info' => 57,
  ),
  314 => 
  array (
    'mark' => 'QP8',
    'unit' => 'm3/h',
    'exponent' => 0,
    'variable' => 9,
    'info' => 57,
  ),
  315 => 
  array (
    'mark' => 'QP9',
    'unit' => 'm3/h',
    'exponent' => 0,
    'variable' => 9,
    'info' => 57,
  ),
  316 => 
  array (
    'mark' => 'QP10',
    'unit' => 'm3/h',
    'exponent' => 0,
    'variable' => 9,
    'info' => 57,
  ),
  317 => 
  array (
    'mark' => 'F_Q',
    'unit' => '',
    'exponent' => 0,
    'variable' => 9,
    'info' => 25,
  ),
  318 => 
  array (
    'mark' => 'Vb_N',
    'unit' => 'm3',
    'exponent' => 0,
    'variable' => 9,
    'info' => 25,
  ),
  319 => 
  array (
    'mark' => 'Vc',
    'unit' => 'm3',
    'exponent' => 0,
    'variable' => 9,
    'info' => 25,
  ),
  320 => 
  array (
    'mark' => 'E_N',
    'unit' => 'kWh',
    'exponent' => 0,
    'variable' => 9,
    'info' => 25,
  ),
  321 => 
  array (
    'mark' => 'M_N',
    'unit' => 'kg',
    'exponent' => 0,
    'variable' => 9,
    'info' => 25,
  ),
  322 => 
  array (
    'mark' => 'Qb_N',
    'unit' => 'm3/h',
    'exponent' => 0,
    'variable' => 7,
    'info' => 23,
  ),
  323 => 
  array (
    'mark' => 'Qm_N',
    'unit' => 'm3/h',
    'exponent' => 0,
    'variable' => 7,
    'info' => 23,
  ),
  324 => 
  array (
    'mark' => 'Vb kb',
    'unit' => 'm3',
    'exponent' => 0,
    'variable' => 9,
    'info' => 25,
  ),
  325 => 
  array (
    'mark' => 'Vm kb',
    'unit' => 'm3',
    'exponent' => 0,
    'variable' => 9,
    'info' => 25,
  ),
  326 => 
  array (
    'mark' => 'E kb',
    'unit' => 'kWh',
    'exponent' => 0,
    'variable' => 9,
    'info' => 25,
  ),
  327 => 
  array (
    'mark' => 'M kb',
    'unit' => 'kg',
    'exponent' => 0,
    'variable' => 9,
    'info' => 25,
  ),
  328 => 
  array (
    'mark' => '---',
    'unit' => '',
    'exponent' => 0,
    'variable' => 9,
    'info' => 25,
  ),
  329 => 
  array (
    'mark' => 'Alarm 1 D',
    'unit' => '',
    'exponent' => 0,
    'variable' => 6,
    'info' => 150,
  ),
  330 => 
  array (
    'mark' => 'Alarm 2 D',
    'unit' => '',
    'exponent' => 0,
    'variable' => 6,
    'info' => 150,
  ),
  331 => 
  array (
    'mark' => 'Alarm 3 D',
    'unit' => '',
    'exponent' => 0,
    'variable' => 6,
    'info' => 150,
  ),
  332 => 
  array (
    'mark' => '---',
    'unit' => '',
    'exponent' => 0,
    'variable' => 7,
    'info' => 23,
  ),
  333 => 
  array (
    'mark' => '---',
    'unit' => '',
    'exponent' => 0,
    'variable' => 7,
    'info' => 23,
  ),
  334 => 
  array (
    'mark' => '---',
    'unit' => '',
    'exponent' => 0,
    'variable' => 7,
    'info' => 23,
  ),
  335 => 
  array (
    'mark' => '---',
    'unit' => '',
    'exponent' => 0,
    'variable' => 7,
    'info' => 23,
  ),
  336 => 
  array (
    'mark' => '---',
    'unit' => '',
    'exponent' => 0,
    'variable' => 7,
    'info' => 23,
  ),
  337 => 
  array (
    'mark' => '---',
    'unit' => '',
    'exponent' => 0,
    'variable' => 7,
    'info' => 23,
  ),
  338 => 
  array (
    'mark' => '---',
    'unit' => '',
    'exponent' => 0,
    'variable' => 6,
    'info' => 22,
  ),
  339 => 
  array (
    'mark' => '---',
    'unit' => '',
    'exponent' => 0,
    'variable' => 12,
    'info' => 28,
  ),
  340 => 
  array (
    'mark' => '---',
    'unit' => '',
    'exponent' => 0,
    'variable' => 12,
    'info' => 28,
  ),
  341 => 
  array (
    'mark' => '---',
    'unit' => '',
    'exponent' => 0,
    'variable' => 12,
    'info' => 28,
  ),
  342 => 
  array (
    'mark' => '---',
    'unit' => '',
    'exponent' => 0,
    'variable' => 12,
    'info' => 28,
  ),
  343 => 
  array (
    'mark' => '---',
    'unit' => '',
    'exponent' => 0,
    'variable' => 12,
    'info' => 28,
  ),
  344 => 
  array (
    'mark' => '---',
    'unit' => '',
    'exponent' => 0,
    'variable' => 12,
    'info' => 28,
  ),
  345 => 
  array (
    'mark' => '---',
    'unit' => '',
    'exponent' => 0,
    'variable' => 6,
    'info' => 22,
  ),
  346 => 
  array (
    'mark' => '---',
    'unit' => '',
    'exponent' => 0,
    'variable' => 6,
    'info' => 22,
  ),
  347 => 
  array (
    'mark' => '---',
    'unit' => '',
    'exponent' => 0,
    'variable' => 6,
    'info' => 22,
  ),
  348 => 
  array (
    'mark' => '---',
    'unit' => '',
    'exponent' => 0,
    'variable' => 6,
    'info' => 22,
  ),
  349 => 
  array (
    'mark' => '---',
    'unit' => '',
    'exponent' => 0,
    'variable' => 6,
    'info' => 22,
  ),
  350 => 
  array (
    'mark' => '---',
    'unit' => '',
    'exponent' => 0,
    'variable' => 6,
    'info' => 22,
  ),
  351 => 
  array (
    'mark' => 'USER-0 pass.',
    'unit' => '',
    'exponent' => 0,
    'variable' => 12,
    'info' => 60,
  ),
  352 => 
  array (
    'mark' => 'USER-1 pass.',
    'unit' => '',
    'exponent' => 0,
    'variable' => 12,
    'info' => 60,
  ),
  353 => 
  array (
    'mark' => '---',
    'unit' => '',
    'exponent' => 0,
    'variable' => 12,
    'info' => 28,
  ),
  354 => 
  array (
    'mark' => '---',
    'unit' => '',
    'exponent' => 0,
    'variable' => 12,
    'info' => 28,
  ),
  355 => 
  array (
    'mark' => '---',
    'unit' => '',
    'exponent' => 0,
    'variable' => 12,
    'info' => 28,
  ),
  356 => 
  array (
    'mark' => '---',
    'unit' => '',
    'exponent' => 0,
    'variable' => 12,
    'info' => 28,
  ),
  357 => 
  array (
    'mark' => 'al_GA1',
    'unit' => '',
    'exponent' => 0,
    'variable' => 6,
    'info' => 54,
  ),
  358 => 
  array (
    'mark' => 'al_GA2',
    'unit' => '',
    'exponent' => 0,
    'variable' => 6,
    'info' => 54,
  ),
  359 => 
  array (
    'mark' => 'al_GA3',
    'unit' => '',
    'exponent' => 0,
    'variable' => 6,
    'info' => 54,
  ),
  360 => 
  array (
    'mark' => 'al_GA time',
    'unit' => 's',
    'exponent' => 0,
    'variable' => 5,
    'info' => 53,
  ),
  361 => 
  array (
    'mark' => 'al_GB1',
    'unit' => '',
    'exponent' => 0,
    'variable' => 6,
    'info' => 54,
  ),
  362 => 
  array (
    'mark' => 'al_GB2',
    'unit' => '',
    'exponent' => 0,
    'variable' => 6,
    'info' => 54,
  ),
  363 => 
  array (
    'mark' => 'al_GB3',
    'unit' => '',
    'exponent' => 0,
    'variable' => 6,
    'info' => 54,
  ),
  364 => 
  array (
    'mark' => 'al_GB time',
    'unit' => 's',
    'exponent' => 0,
    'variable' => 5,
    'info' => 53,
  ),
  365 => 
  array (
    'mark' => 'conf_sens',
    'unit' => '',
    'exponent' => 0,
    'variable' => 5,
    'info' => 53,
  ),
  366 => 
  array (
    'mark' => 'NS1',
    'unit' => '',
    'exponent' => 0,
    'variable' => 12,
    'info' => 60,
  ),
  367 => 
  array (
    'mark' => 'NS2',
    'unit' => '',
    'exponent' => 0,
    'variable' => 12,
    'info' => 60,
  ),
  368 => 
  array (
    'mark' => 'NS3',
    'unit' => '',
    'exponent' => 0,
    'variable' => 12,
    'info' => 60,
  ),
  369 => 
  array (
    'mark' => 'NS4',
    'unit' => '',
    'exponent' => 0,
    'variable' => 12,
    'info' => 60,
  ),
  370 => 
  array (
    'mark' => 'NS5',
    'unit' => '',
    'exponent' => 0,
    'variable' => 12,
    'info' => 60,
  ),
  371 => 
  array (
    'mark' => 'NS6',
    'unit' => '',
    'exponent' => 0,
    'variable' => 12,
    'info' => 60,
  ),
  372 => 
  array (
    'mark' => 'NS7',
    'unit' => '',
    'exponent' => 0,
    'variable' => 12,
    'info' => 60,
  ),
  373 => 
  array (
    'mark' => 'NS8',
    'unit' => '',
    'exponent' => 0,
    'variable' => 12,
    'info' => 60,
  ),
  374 => 
  array (
    'mark' => '---',
    'unit' => '',
    'exponent' => 0,
    'variable' => 5,
    'info' => 21,
  ),
  375 => 
  array (
    'mark' => 'l dEh 1',
    'unit' => 'kWh',
    'exponent' => 0,
    'variable' => 9,
    'info' => 57,
  ),
  376 => 
  array (
    'mark' => 'l dEh 2',
    'unit' => 'kWh',
    'exponent' => 0,
    'variable' => 9,
    'info' => 57,
  ),
  377 => 
  array (
    'mark' => 'l dEh 3',
    'unit' => 'kWh',
    'exponent' => 0,
    'variable' => 9,
    'info' => 57,
  ),
  378 => 
  array (
    'mark' => 'eph time',
    'unit' => 's',
    'exponent' => 0,
    'variable' => 5,
    'info' => 53,
  ),
  379 => 
  array (
    'mark' => 't limit 1',
    'unit' => 'min',
    'exponent' => 0,
    'variable' => 5,
    'info' => 53,
  ),
  380 => 
  array (
    'mark' => 't limit 2',
    'unit' => 'min',
    'exponent' => 0,
    'variable' => 5,
    'info' => 53,
  ),
  381 => 
  array (
    'mark' => 'l0 dVbh 1',
    'unit' => '%',
    'exponent' => 0,
    'variable' => 9,
    'info' => 57,
  ),
  382 => 
  array (
    'mark' => 'l0 dVbh 2',
    'unit' => '%',
    'exponent' => 0,
    'variable' => 9,
    'info' => 57,
  ),
  383 => 
  array (
    'mark' => 'cl dVbh 1',
    'unit' => 'm3',
    'exponent' => 0,
    'variable' => 9,
    'info' => 25,
  ),
  384 => 
  array (
    'mark' => 'cl dVbh 2',
    'unit' => 'm3',
    'exponent' => 0,
    'variable' => 9,
    'info' => 25,
  ),
  385 => 
  array (
    'mark' => 'Alarm 3',
    'unit' => '',
    'exponent' => 0,
    'variable' => 6,
    'info' => 22,
  ),
  386 => 
  array (
    'mark' => 'lock MET',
    'unit' => '',
    'exponent' => 0,
    'variable' => 4,
    'info' => 52,
  ),
  387 => 
  array (
    'mark' => 'Index AS',
    'unit' => '',
    'exponent' => 0,
    'variable' => 6,
    'info' => 22,
  ),
  388 => 
  array (
    'mark' => 'AlarmLOG',
    'unit' => '%',
    'exponent' => 0,
    'variable' => 7,
    'info' => 55,
  ),
  389 => 
  array (
    'mark' => 'SetupLOG',
    'unit' => '%',
    'exponent' => 0,
    'variable' => 7,
    'info' => 55,
  ),
  390 => 
  array (
    'mark' => 'l eph 1',
    'unit' => 'm3',
    'exponent' => 0,
    'variable' => 9,
    'info' => 57,
  ),
  391 => 
  array (
    'mark' => 'l eph 2',
    'unit' => 'm3',
    'exponent' => 0,
    'variable' => 9,
    'info' => 57,
  ),
  392 => 
  array (
    'mark' => 'p1w min',
    'unit' => 'kPa',
    'exponent' => 0,
    'variable' => 9,
    'info' => 57,
  ),
  393 => 
  array (
    'mark' => 'p1w max',
    'unit' => 'kPa',
    'exponent' => 0,
    'variable' => 9,
    'info' => 57,
  ),
  394 => 
  array (
    'mark' => 'p2w min',
    'unit' => 'kPa',
    'exponent' => 0,
    'variable' => 9,
    'info' => 57,
  ),
  395 => 
  array (
    'mark' => 'p2w max',
    'unit' => 'kPa',
    'exponent' => 0,
    'variable' => 9,
    'info' => 57,
  ),
  396 => 
  array (
    'mark' => 'tw min',
    'unit' => '°C',
    'exponent' => 0,
    'variable' => 9,
    'info' => 57,
  ),
  397 => 
  array (
    'mark' => 'tw max',
    'unit' => '°C',
    'exponent' => 0,
    'variable' => 9,
    'info' => 57,
  ),
  398 => 
  array (
    'mark' => 'p1a',
    'unit' => '',
    'exponent' => 0,
    'variable' => 7,
    'info' => 55,
  ),
  399 => 
  array (
    'mark' => 'p1b',
    'unit' => '',
    'exponent' => 0,
    'variable' => 7,
    'info' => 55,
  ),
  400 => 
  array (
    'mark' => 'ta',
    'unit' => '',
    'exponent' => 0,
    'variable' => 7,
    'info' => 55,
  ),
  401 => 
  array (
    'mark' => 'tb',
    'unit' => '',
    'exponent' => 0,
    'variable' => 7,
    'info' => 55,
  ),
  402 => 
  array (
    'mark' => 'p2a',
    'unit' => '',
    'exponent' => 0,
    'variable' => 7,
    'info' => 55,
  ),
  403 => 
  array (
    'mark' => 'p2b',
    'unit' => '',
    'exponent' => 0,
    'variable' => 7,
    'info' => 55,
  ),
  404 => 
  array (
    'mark' => 'conf_language',
    'unit' => '',
    'exponent' => 0,
    'variable' => 4,
    'info' => 52,
  ),
  405 => 
  array (
    'mark' => 'languages',
    'unit' => '',
    'exponent' => 0,
    'variable' => 12,
    'info' => 28,
  ),
  406 => 
  array (
    'mark' => 'Qm time',
    'unit' => 'min',
    'exponent' => 0,
    'variable' => 4,
    'info' => 52,
  ),
  407 => 
  array (
    'mark' => 'OTS',
    'unit' => '',
    'exponent' => 0,
    'variable' => 6,
    'info' => 22,
  ),
  408 => 
  array (
    'mark' => 'PRV',
    'unit' => '',
    'exponent' => 0,
    'variable' => 5,
    'info' => 21,
  ),
  409 => 
  array (
    'mark' => 'Index D',
    'unit' => '',
    'exponent' => 0,
    'variable' => 6,
    'info' => 22,
  ),
  410 => 
  array (
    'mark' => 'PRP1',
    'unit' => '',
    'exponent' => 0,
    'variable' => 5,
    'info' => 53,
  ),
  411 => 
  array (
    'mark' => 'PRP2',
    'unit' => '',
    'exponent' => 0,
    'variable' => 5,
    'info' => 53,
  ),
  412 => 
  array (
    'mark' => 'PRP3',
    'unit' => '',
    'exponent' => 0,
    'variable' => 5,
    'info' => 53,
  ),
  413 => 
  array (
    'mark' => 'PRP4',
    'unit' => '',
    'exponent' => 0,
    'variable' => 5,
    'info' => 53,
  ),
  414 => 
  array (
    'mark' => 'PRP5',
    'unit' => '',
    'exponent' => 0,
    'variable' => 5,
    'info' => 53,
  ),
  415 => 
  array (
    'mark' => 'PRP6',
    'unit' => '',
    'exponent' => 0,
    'variable' => 5,
    'info' => 53,
  ),
  416 => 
  array (
    'mark' => 'PRP7',
    'unit' => '',
    'exponent' => 0,
    'variable' => 5,
    'info' => 53,
  ),
  417 => 
  array (
    'mark' => 'PRP8',
    'unit' => '',
    'exponent' => 0,
    'variable' => 5,
    'info' => 53,
  ),
  418 => 
  array (
    'mark' => 'PRP9',
    'unit' => '',
    'exponent' => 0,
    'variable' => 5,
    'info' => 53,
  ),
  419 => 
  array (
    'mark' => 'PRP10',
    'unit' => '',
    'exponent' => 0,
    'variable' => 5,
    'info' => 53,
  ),
  420 => 
  array (
    'mark' => 'PRP11',
    'unit' => '',
    'exponent' => 0,
    'variable' => 5,
    'info' => 53,
  ),
  421 => 
  array (
    'mark' => 'PRP12',
    'unit' => '',
    'exponent' => 0,
    'variable' => 5,
    'info' => 53,
  ),
  422 => 
  array (
    'mark' => 'PRP13',
    'unit' => '',
    'exponent' => 0,
    'variable' => 5,
    'info' => 53,
  ),
  423 => 
  array (
    'mark' => 'PRP14',
    'unit' => '',
    'exponent' => 0,
    'variable' => 5,
    'info' => 53,
  ),
  424 => 
  array (
    'mark' => 'PRP15',
    'unit' => '',
    'exponent' => 0,
    'variable' => 5,
    'info' => 53,
  ),
  425 => 
  array (
    'mark' => 'PRP16',
    'unit' => '',
    'exponent' => 0,
    'variable' => 5,
    'info' => 53,
  ),
  426 => 
  array (
    'mark' => 'PRP17',
    'unit' => '',
    'exponent' => 0,
    'variable' => 5,
    'info' => 53,
  ),
  427 => 
  array (
    'mark' => 'PRP18',
    'unit' => '',
    'exponent' => 0,
    'variable' => 5,
    'info' => 53,
  ),
  428 => 
  array (
    'mark' => 'PRP19',
    'unit' => '',
    'exponent' => 0,
    'variable' => 5,
    'info' => 53,
  ),
  429 => 
  array (
    'mark' => 'PRP20',
    'unit' => '',
    'exponent' => 0,
    'variable' => 5,
    'info' => 53,
  ),
  430 => 
  array (
    'mark' => 'PRP21',
    'unit' => '',
    'exponent' => 0,
    'variable' => 5,
    'info' => 53,
  ),
  431 => 
  array (
    'mark' => 'PRP22',
    'unit' => '',
    'exponent' => 0,
    'variable' => 5,
    'info' => 53,
  ),
  432 => 
  array (
    'mark' => 'PRP23',
    'unit' => '',
    'exponent' => 0,
    'variable' => 5,
    'info' => 53,
  ),
  433 => 
  array (
    'mark' => 'PRP24',
    'unit' => '',
    'exponent' => 0,
    'variable' => 5,
    'info' => 53,
  ),
  434 => 
  array (
    'mark' => 'PRP25',
    'unit' => '',
    'exponent' => 0,
    'variable' => 5,
    'info' => 53,
  ),
  435 => 
  array (
    'mark' => 'PRP26',
    'unit' => '',
    'exponent' => 0,
    'variable' => 5,
    'info' => 53,
  ),
  436 => 
  array (
    'mark' => 'PRP27',
    'unit' => '',
    'exponent' => 0,
    'variable' => 5,
    'info' => 53,
  ),
  437 => 
  array (
    'mark' => 'PRP28',
    'unit' => '',
    'exponent' => 0,
    'variable' => 5,
    'info' => 53,
  ),
  438 => 
  array (
    'mark' => 'PRP29',
    'unit' => '',
    'exponent' => 0,
    'variable' => 5,
    'info' => 53,
  ),
  439 => 
  array (
    'mark' => 'PRP30',
    'unit' => '',
    'exponent' => 0,
    'variable' => 5,
    'info' => 53,
  ),
  440 => 
  array (
    'mark' => 'EXT POW',
    'unit' => '',
    'exponent' => 0,
    'variable' => 4,
    'info' => 20,
  ),
  441 => 
  array (
    'mark' => 'EXT_POW_CHECK',
    'unit' => '',
    'exponent' => 0,
    'variable' => 4,
    'info' => 52,
  ),
  442 => 
  array (
    'mark' => 'CRC1',
    'unit' => '',
    'exponent' => 0,
    'variable' => 6,
    'info' => 22,
  ),
  443 => 
  array (
    'mark' => 'CRC2',
    'unit' => '',
    'exponent' => 0,
    'variable' => 6,
    'info' => 22,
  ),
  444 => 
  array (
    'mark' => 'CRC3',
    'unit' => '',
    'exponent' => 0,
    'variable' => 6,
    'info' => 22,
  ),
  445 => 
  array (
    'mark' => 'CRC4',
    'unit' => '',
    'exponent' => 0,
    'variable' => 6,
    'info' => 22,
  ),
  446 => 
  array (
    'mark' => 'CRC5',
    'unit' => '',
    'exponent' => 0,
    'variable' => 6,
    'info' => 22,
  ),
  447 => 
  array (
    'mark' => 'CRC6',
    'unit' => '',
    'exponent' => 0,
    'variable' => 6,
    'info' => 22,
  ),
  448 => 
  array (
    'mark' => 'CRC7',
    'unit' => '',
    'exponent' => 0,
    'variable' => 6,
    'info' => 22,
  ),
  449 => 
  array (
    'mark' => 'CRC8',
    'unit' => '',
    'exponent' => 0,
    'variable' => 6,
    'info' => 22,
  ),
  450 => 
  array (
    'mark' => 'CRC9',
    'unit' => '',
    'exponent' => 0,
    'variable' => 6,
    'info' => 22,
  ),
  451 => 
  array (
    'mark' => 'CRC10',
    'unit' => '',
    'exponent' => 0,
    'variable' => 6,
    'info' => 22,
  ),
  452 => 
  array (
    'mark' => 'FRC1',
    'unit' => '',
    'exponent' => 0,
    'variable' => 6,
    'info' => 54,
  ),
  453 => 
  array (
    'mark' => 'FSC1',
    'unit' => '',
    'exponent' => 0,
    'variable' => 6,
    'info' => 54,
  ),
  454 => 
  array (
    'mark' => 'AFRC1',
    'unit' => '',
    'exponent' => 0,
    'variable' => 6,
    'info' => 54,
  ),
  455 => 
  array (
    'mark' => 'FRC2',
    'unit' => '',
    'exponent' => 0,
    'variable' => 6,
    'info' => 54,
  ),
  456 => 
  array (
    'mark' => 'FSC2',
    'unit' => '',
    'exponent' => 0,
    'variable' => 6,
    'info' => 54,
  ),
  457 => 
  array (
    'mark' => 'AFRC2',
    'unit' => '',
    'exponent' => 0,
    'variable' => 6,
    'info' => 54,
  ),
  458 => 
  array (
    'mark' => 'FRC3',
    'unit' => '',
    'exponent' => 0,
    'variable' => 6,
    'info' => 54,
  ),
  459 => 
  array (
    'mark' => 'FSC3',
    'unit' => '',
    'exponent' => 0,
    'variable' => 6,
    'info' => 54,
  ),
  460 => 
  array (
    'mark' => 'AFRC3',
    'unit' => '',
    'exponent' => 0,
    'variable' => 6,
    'info' => 54,
  ),
  461 => 
  array (
    'mark' => 'sense',
    'unit' => '',
    'exponent' => 0,
    'variable' => 7,
    'info' => 23,
  ),
  462 => 
  array (
    'mark' => 'sense V_in',
    'unit' => 'V',
    'exponent' => 0,
    'variable' => 7,
    'info' => 23,
  ),
  463 => 
  array (
    'mark' => 'sense min',
    'unit' => '',
    'exponent' => 0,
    'variable' => 7,
    'info' => 55,
  ),
  464 => 
  array (
    'mark' => 'sense max',
    'unit' => '',
    'exponent' => 0,
    'variable' => 7,
    'info' => 55,
  ),
  465 => 
  array (
    'mark' => 'sense a',
    'unit' => '',
    'exponent' => 0,
    'variable' => 7,
    'info' => 55,
  ),
  466 => 
  array (
    'mark' => 'sense b',
    'unit' => '',
    'exponent' => 0,
    'variable' => 7,
    'info' => 55,
  ),
  467 => 
  array (
    'mark' => 'sense on',
    'unit' => '',
    'exponent' => 0,
    'variable' => 4,
    'info' => 52,
  ),
  468 => 
  array (
    'mark' => 'io',
    'unit' => 'mA',
    'exponent' => 0,
    'variable' => 7,
    'info' => 23,
  ),
  469 => 
  array (
    'mark' => 'io idx',
    'unit' => '',
    'exponent' => 0,
    'variable' => 5,
    'info' => 53,
  ),
  470 => 
  array (
    'mark' => 'io min',
    'unit' => '',
    'exponent' => 0,
    'variable' => 7,
    'info' => 55,
  ),
  471 => 
  array (
    'mark' => 'io max',
    'unit' => '',
    'exponent' => 0,
    'variable' => 7,
    'info' => 55,
  ),
  472 => 
  array (
    'mark' => 'io a',
    'unit' => '',
    'exponent' => 0,
    'variable' => 7,
    'info' => 55,
  ),
  473 => 
  array (
    'mark' => 'io b',
    'unit' => '',
    'exponent' => 0,
    'variable' => 7,
    'info' => 55,
  ),
  474 => 
  array (
    'mark' => 'io on',
    'unit' => '',
    'exponent' => 0,
    'variable' => 4,
    'info' => 52,
  ),
  475 => 
  array (
    'mark' => 'Calib',
    'unit' => '',
    'exponent' => 0,
    'variable' => 4,
    'info' => 20,
  ),
  476 => 
  array (
    'mark' => 'SV1',
    'unit' => '',
    'exponent' => 0,
    'variable' => 5,
    'info' => 21,
  ),
  477 => 
  array (
    'mark' => 'SV2',
    'unit' => '',
    'exponent' => 0,
    'variable' => 5,
    'info' => 21,
  ),
  478 => 
  array (
    'mark' => 'UpCode',
    'unit' => '',
    'exponent' => 0,
    'variable' => 6,
    'info' => 22,
  ),
  479 => 
  array (
    'mark' => 'lock FW1',
    'unit' => '',
    'exponent' => 0,
    'variable' => 4,
    'info' => 52,
  ),
  480 => 
  array (
    'mark' => 'lock FW2',
    'unit' => '',
    'exponent' => 0,
    'variable' => 4,
    'info' => 52,
  ),
  481 => 
  array (
    'mark' => 'SYS1',
    'unit' => '',
    'exponent' => 0,
    'variable' => 6,
    'info' => 86,
  ),
  482 => 
  array (
    'mark' => 'SYS2',
    'unit' => '',
    'exponent' => 0,
    'variable' => 6,
    'info' => 22,
  ),
  483 => 
  array (
    'mark' => 'SYS3',
    'unit' => '',
    'exponent' => 0,
    'variable' => 6,
    'info' => 22,
  ),
  484 => 
  array (
    'mark' => 'SYS4',
    'unit' => '',
    'exponent' => 0,
    'variable' => 6,
    'info' => 22,
  ),
  485 => 
  array (
    'mark' => 'SYS5',
    'unit' => '',
    'exponent' => 0,
    'variable' => 6,
    'info' => 22,
  ),
  486 => 
  array (
    'mark' => 'SYS6',
    'unit' => '',
    'exponent' => 0,
    'variable' => 6,
    'info' => 22,
  ),
  487 => 
  array (
    'mark' => 'Hs min',
    'unit' => 'MJ/m3',
    'exponent' => 0,
    'variable' => 9,
    'info' => 25,
  ),
  488 => 
  array (
    'mark' => 'Hs max',
    'unit' => 'MJ/m3',
    'exponent' => 0,
    'variable' => 9,
    'info' => 25,
  ),
  489 => 
  array (
    'mark' => 'Erasing',
    'unit' => '',
    'exponent' => 0,
    'variable' => 4,
    'info' => 52,
  ),
  490 => 
  array (
    'mark' => 'BCT',
    'unit' => 's',
    'exponent' => 0,
    'variable' => 4,
    'info' => 52,
  ),
  491 => 
  array (
    'mark' => 'ECT',
    'unit' => 's',
    'exponent' => 0,
    'variable' => 4,
    'info' => 52,
  ),
  492 => 
  array (
    'mark' => 'COM1 baudrate',
    'unit' => 'bps',
    'exponent' => 0,
    'variable' => 6,
    'info' => 54,
  ),
  493 => 
  array (
    'mark' => 'COM1 address',
    'unit' => '',
    'exponent' => 0,
    'variable' => 5,
    'info' => 53,
  ),
  494 => 
  array (
    'mark' => 'COM2 baudrate',
    'unit' => 'bps',
    'exponent' => 0,
    'variable' => 6,
    'info' => 54,
  ),
  495 => 
  array (
    'mark' => 'COM2 address',
    'unit' => '',
    'exponent' => 0,
    'variable' => 5,
    'info' => 53,
  ),
  496 => 
  array (
    'mark' => 'COM3 baudrate',
    'unit' => 'bps',
    'exponent' => 0,
    'variable' => 6,
    'info' => 54,
  ),
  497 => 
  array (
    'mark' => 'COM3 address',
    'unit' => '',
    'exponent' => 0,
    'variable' => 5,
    'info' => 53,
  ),
  498 => 
  array (
    'mark' => 'COM3 status',
    'unit' => '',
    'exponent' => 0,
    'variable' => 4,
    'info' => 20,
  ),
  499 => 
  array (
    'mark' => 'dt_tm_comp',
    'unit' => '',
    'exponent' => 0,
    'variable' => 12,
    'info' => 28,
  ),
  500 => 
  array (
    'mark' => 'Resources',
    'unit' => '',
    'exponent' => 0,
    'variable' => 12,
    'info' => 28,
  ),
  501 => 
  array (
    'mark' => 'Cycle',
    'unit' => '',
    'exponent' => 0,
    'variable' => 5,
    'info' => 21,
  ),
  502 => 
  array (
    'mark' => 'LogoutTm',
    'unit' => 'min',
    'exponent' => 0,
    'variable' => 5,
    'info' => 53,
  ),
  503 => 
  array (
    'mark' => 'UC1',
    'unit' => '',
    'exponent' => 0,
    'variable' => 4,
    'info' => 20,
  ),
  504 => 
  array (
    'mark' => 'UC2',
    'unit' => '',
    'exponent' => 0,
    'variable' => 4,
    'info' => 20,
  ),
  505 => 
  array (
    'mark' => 'UC3',
    'unit' => '',
    'exponent' => 0,
    'variable' => 4,
    'info' => 20,
  ),
  506 => 
  array (
    'mark' => 'UC4',
    'unit' => '',
    'exponent' => 0,
    'variable' => 4,
    'info' => 20,
  ),
  507 => 
  array (
    'mark' => 'UC5',
    'unit' => '',
    'exponent' => 0,
    'variable' => 4,
    'info' => 20,
  ),
  508 => 
  array (
    'mark' => 'UC6',
    'unit' => '',
    'exponent' => 0,
    'variable' => 4,
    'info' => 20,
  ),
  509 => 
  array (
    'mark' => 'UC7',
    'unit' => '',
    'exponent' => 0,
    'variable' => 4,
    'info' => 20,
  ),
  510 => 
  array (
    'mark' => 'UC8',
    'unit' => '',
    'exponent' => 0,
    'variable' => 4,
    'info' => 20,
  ),
  511 => 
  array (
    'mark' => 'UC9',
    'unit' => '',
    'exponent' => 0,
    'variable' => 4,
    'info' => 20,
  ),
  512 => 
  array (
    'mark' => 'UC10',
    'unit' => '',
    'exponent' => 0,
    'variable' => 4,
    'info' => 20,
  ),
  513 => 
  array (
    'mark' => 'UC11',
    'unit' => '',
    'exponent' => 0,
    'variable' => 4,
    'info' => 20,
  ),
  514 => 
  array (
    'mark' => 'UC12',
    'unit' => '',
    'exponent' => 0,
    'variable' => 4,
    'info' => 20,
  ),
  515 => 
  array (
    'mark' => 'UC13',
    'unit' => '',
    'exponent' => 0,
    'variable' => 4,
    'info' => 20,
  ),
  516 => 
  array (
    'mark' => 'UC14',
    'unit' => '',
    'exponent' => 0,
    'variable' => 4,
    'info' => 20,
  ),
  517 => 
  array (
    'mark' => 'UC15',
    'unit' => '',
    'exponent' => 0,
    'variable' => 4,
    'info' => 20,
  ),
  518 => 
  array (
    'mark' => 'sys status',
    'unit' => '',
    'exponent' => 0,
    'variable' => 6,
    'info' => 22,
  ),
  519 => 
  array (
    'mark' => 'Batt. cap',
    'unit' => 'Ah',
    'exponent' => 0,
    'variable' => 5,
    'info' => 53,
  ),
  520 => 
  array (
    'mark' => 'Batt. idx',
    'unit' => '',
    'exponent' => 0,
    'variable' => 4,
    'info' => 52,
  ),
);