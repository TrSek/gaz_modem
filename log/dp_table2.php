<?php $dp_table = array (
  0 => 
  array (
    'mark' => 'Vn',
    'unit' => 'm3',
    'exponent' => 0,
    'variable' => 9,
    'info' => 89,
  ),
  1 => 
  array (
    'mark' => 'Vr',
    'unit' => 'm3',
    'exponent' => 0,
    'variable' => 9,
    'info' => 89,
  ),
  2 => 
  array (
    'mark' => 'Qn',
    'unit' => 'm3/h',
    'exponent' => 0,
    'variable' => 7,
    'info' => 87,
  ),
  3 => 
  array (
    'mark' => 'Qr',
    'unit' => 'm3/h',
    'exponent' => 0,
    'variable' => 7,
    'info' => 87,
  ),
  4 => 
  array (
    'mark' => 'dVn',
    'unit' => 'm3',
    'exponent' => 0,
    'variable' => 7,
    'info' => 87,
  ),
  5 => 
  array (
    'mark' => 'dVr',
    'unit' => 'm3',
    'exponent' => 0,
    'variable' => 7,
    'info' => 87,
  ),
  6 => 
  array (
    'mark' => 'Qm',
    'unit' => 'kg/h',
    'exponent' => 0,
    'variable' => 7,
    'info' => 23,
  ),
  7 => 
  array (
    'mark' => 'dM',
    'unit' => 'kg',
    'exponent' => 0,
    'variable' => 7,
    'info' => 23,
  ),
  8 => 
  array (
    'mark' => 'M',
    'unit' => 'kg',
    'exponent' => 0,
    'variable' => 9,
    'info' => 89,
  ),
  9 => 
  array (
    'mark' => 'QE',
    'unit' => 'MJ/h',
    'exponent' => 0,
    'variable' => 7,
    'info' => 23,
  ),
  10 => 
  array (
    'mark' => 'dE',
    'unit' => 'MJ',
    'exponent' => 0,
    'variable' => 7,
    'info' => 23,
  ),
  11 => 
  array (
    'mark' => 'E',
    'unit' => 'MJ',
    'exponent' => 0,
    'variable' => 7,
    'info' => 23,
  ),
  12 => 
  array (
    'mark' => 'qh',
    'unit' => 'm3/h',
    'exponent' => 0,
    'variable' => 7,
    'info' => 87,
  ),
  13 => 
  array (
    'mark' => 'dP',
    'unit' => 'Pa',
    'exponent' => 0,
    'variable' => 7,
    'info' => 87,
  ),
  14 => 
  array (
    'mark' => 'P',
    'unit' => 'Pa',
    'exponent' => 0,
    'variable' => 7,
    'info' => 87,
  ),
  15 => 
  array (
    'mark' => 'dPmin',
    'unit' => 'Pa',
    'exponent' => 0,
    'variable' => 7,
    'info' => 55,
  ),
  16 => 
  array (
    'mark' => 'pPmax',
    'unit' => 'Pa',
    'exponent' => 0,
    'variable' => 7,
    'info' => 55,
  ),
  17 => 
  array (
    'mark' => 'Pmin',
    'unit' => 'Pa',
    'exponent' => 0,
    'variable' => 7,
    'info' => 55,
  ),
  18 => 
  array (
    'mark' => 'Pmax',
    'unit' => 'Pa',
    'exponent' => 0,
    'variable' => 7,
    'info' => 55,
  ),
  19 => 
  array (
    'mark' => 't',
    'unit' => '�C',
    'exponent' => 0,
    'variable' => 7,
    'info' => 87,
  ),
  20 => 
  array (
    'mark' => 'tmin',
    'unit' => '�C',
    'exponent' => 0,
    'variable' => 7,
    'info' => 55,
  ),
  21 => 
  array (
    'mark' => 'tmax',
    'unit' => '�C',
    'exponent' => 0,
    'variable' => 7,
    'info' => 55,
  ),
  22 => 
  array (
    'mark' => 'Qrmin',
    'unit' => 'm3/h',
    'exponent' => 0,
    'variable' => 7,
    'info' => 55,
  ),
  23 => 
  array (
    'mark' => 'Qrmax',
    'unit' => 'm3/h',
    'exponent' => 0,
    'variable' => 7,
    'info' => 55,
  ),
  24 => 
  array (
    'mark' => 'd',
    'unit' => 'm',
    'exponent' => 0,
    'variable' => 7,
    'info' => 55,
  ),
  25 => 
  array (
    'mark' => 'D',
    'unit' => 'm',
    'exponent' => 0,
    'variable' => 7,
    'info' => 55,
  ),
  26 => 
  array (
    'mark' => 'alfatk',
    'unit' => 'C-1',
    'exponent' => 0,
    'variable' => 7,
    'info' => 55,
  ),
  27 => 
  array (
    'mark' => 'alfatr',
    'unit' => 'C-1',
    'exponent' => 0,
    'variable' => 7,
    'info' => 55,
  ),
  28 => 
  array (
    'mark' => 'XC1',
    'unit' => '%',
    'exponent' => 0,
    'variable' => 7,
    'info' => 55,
  ),
  29 => 
  array (
    'mark' => 'XC2',
    'unit' => '%',
    'exponent' => 0,
    'variable' => 7,
    'info' => 55,
  ),
  30 => 
  array (
    'mark' => 'XC3',
    'unit' => '%',
    'exponent' => 0,
    'variable' => 7,
    'info' => 55,
  ),
  31 => 
  array (
    'mark' => 'Xn-C4',
    'unit' => '%',
    'exponent' => 0,
    'variable' => 7,
    'info' => 55,
  ),
  32 => 
  array (
    'mark' => 'Xi-C4',
    'unit' => '%',
    'exponent' => 0,
    'variable' => 7,
    'info' => 55,
  ),
  33 => 
  array (
    'mark' => 'Xneo-C5',
    'unit' => '%',
    'exponent' => 0,
    'variable' => 7,
    'info' => 55,
  ),
  34 => 
  array (
    'mark' => 'Xn-C5',
    'unit' => '%',
    'exponent' => 0,
    'variable' => 7,
    'info' => 55,
  ),
  35 => 
  array (
    'mark' => 'Xi-C5',
    'unit' => '%',
    'exponent' => 0,
    'variable' => 7,
    'info' => 55,
  ),
  36 => 
  array (
    'mark' => 'XC6+',
    'unit' => '%',
    'exponent' => 0,
    'variable' => 7,
    'info' => 55,
  ),
  37 => 
  array (
    'mark' => 'XN2',
    'unit' => '%',
    'exponent' => 0,
    'variable' => 7,
    'info' => 55,
  ),
  38 => 
  array (
    'mark' => 'XCO2',
    'unit' => '%',
    'exponent' => 0,
    'variable' => 7,
    'info' => 55,
  ),
  39 => 
  array (
    'mark' => 'mi',
    'unit' => 'kg/ms',
    'exponent' => 0,
    'variable' => 7,
    'info' => 55,
  ),
  40 => 
  array (
    'mark' => 'ror',
    'unit' => '',
    'exponent' => 0,
    'variable' => 7,
    'info' => 55,
  ),
  41 => 
  array (
    'mark' => 'Hs',
    'unit' => 'MJ/m3',
    'exponent' => 0,
    'variable' => 7,
    'info' => 55,
  ),
  42 => 
  array (
    'mark' => 'Z',
    'unit' => '',
    'exponent' => 0,
    'variable' => 7,
    'info' => 23,
  ),
  43 => 
  array (
    'mark' => 'K1',
    'unit' => '',
    'exponent' => 0,
    'variable' => 7,
    'info' => 23,
  ),
  44 => 
  array (
    'mark' => 'K',
    'unit' => '',
    'exponent' => 0,
    'variable' => 7,
    'info' => 23,
  ),
  45 => 
  array (
    'mark' => 'Re',
    'unit' => '',
    'exponent' => 0,
    'variable' => 7,
    'info' => 23,
  ),
  46 => 
  array (
    'mark' => 'bet',
    'unit' => '',
    'exponent' => 0,
    'variable' => 7,
    'info' => 23,
  ),
  47 => 
  array (
    'mark' => 'kap',
    'unit' => '',
    'exponent' => 0,
    'variable' => 7,
    'info' => 23,
  ),
  48 => 
  array (
    'mark' => 'eps',
    'unit' => '',
    'exponent' => 0,
    'variable' => 7,
    'info' => 23,
  ),
  49 => 
  array (
    'mark' => '�C',
    'unit' => '',
    'exponent' => 0,
    'variable' => 7,
    'info' => 23,
  ),
  50 => 
  array (
    'mark' => 'dtau',
    'unit' => 's',
    'exponent' => 0,
    'variable' => 7,
    'info' => 55,
  ),
  51 => 
  array (
    'mark' => 'Ident',
    'unit' => '',
    'exponent' => 0,
    'variable' => 7,
    'info' => 23,
  ),
  52 => 
  array (
    'mark' => 'Nrpar',
    'unit' => '',
    'exponent' => 0,
    'variable' => 7,
    'info' => 87,
  ),
  53 => 
  array (
    'mark' => 'Vnaw',
    'unit' => 'm3',
    'exponent' => 0,
    'variable' => 9,
    'info' => 89,
  ),
  54 => 
  array (
    'mark' => 'Vraw',
    'unit' => 'm3',
    'exponent' => 0,
    'variable' => 9,
    'info' => 89,
  ),
  55 => 
  array (
    'mark' => 'dVnaw',
    'unit' => 'm3',
    'exponent' => 0,
    'variable' => 7,
    'info' => 87,
  ),
  56 => 
  array (
    'mark' => 'dVraw',
    'unit' => 'm3',
    'exponent' => 0,
    'variable' => 7,
    'info' => 23,
  ),
);