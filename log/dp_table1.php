<?php $dp_table = array (
  0 => 
  array (
    'mark' => 'Vn0',
    'unit' => 'm3',
    'exponent' => 0,
    'variable' => 7,
    'info' => 1,
  ),
  1 => 
  array (
    'mark' => 'Vn1',
    'unit' => 'm3',
    'exponent' => 0,
    'variable' => 7,
    'info' => 1,
  ),
  2 => 
  array (
    'mark' => 'Qn',
    'unit' => 'm3/h',
    'exponent' => 0,
    'variable' => 7,
    'info' => 1,
  ),
  3 => 
  array (
    'mark' => 'Qr',
    'unit' => 'm3/h',
    'exponent' => 0,
    'variable' => 7,
    'info' => 1,
  ),
  4 => 
  array (
    'mark' => 'dP',
    'unit' => 'kPa',
    'exponent' => 0,
    'variable' => 7,
    'info' => 7,
  ),
  5 => 
  array (
    'mark' => 'P',
    'unit' => 'kPa',
    'exponent' => 0,
    'variable' => 7,
    'info' => 7,
  ),
  6 => 
  array (
    'mark' => 't',
    'unit' => '°C',
    'exponent' => 0,
    'variable' => 7,
    'info' => 7,
  ),
  7 => 
  array (
    'mark' => 'rez1',
    'unit' => '',
    'exponent' => 0,
    'variable' => 7,
    'info' => 7,
  ),
  8 => 
  array (
    'mark' => 'rez2',
    'unit' => '',
    'exponent' => 0,
    'variable' => 7,
    'info' => 7,
  ),
  9 => 
  array (
    'mark' => 'Qmin',
    'unit' => 'm3/h',
    'exponent' => 0,
    'variable' => 7,
    'info' => 16,
  ),
  10 => 
  array (
    'mark' => 'Qmax',
    'unit' => 'm3/h',
    'exponent' => 0,
    'variable' => 7,
    'info' => 16,
  ),
  11 => 
  array (
    'mark' => 'dPmin',
    'unit' => 'kPa',
    'exponent' => 0,
    'variable' => 7,
    'info' => 16,
  ),
  12 => 
  array (
    'mark' => 'dPmax',
    'unit' => 'kPa',
    'exponent' => 0,
    'variable' => 7,
    'info' => 16,
  ),
  13 => 
  array (
    'mark' => 'Pmin',
    'unit' => 'kPa',
    'exponent' => 0,
    'variable' => 7,
    'info' => 16,
  ),
  14 => 
  array (
    'mark' => 'Pmax',
    'unit' => 'kPa',
    'exponent' => 0,
    'variable' => 7,
    'info' => 16,
  ),
  15 => 
  array (
    'mark' => 'tmin',
    'unit' => '°C',
    'exponent' => 0,
    'variable' => 7,
    'info' => 16,
  ),
  16 => 
  array (
    'mark' => 'tmax',
    'unit' => '°C',
    'exponent' => 0,
    'variable' => 7,
    'info' => 16,
  ),
  17 => 
  array (
    'mark' => 'rez1 min',
    'unit' => '',
    'exponent' => 0,
    'variable' => 7,
    'info' => 16,
  ),
  18 => 
  array (
    'mark' => 'rez1 max',
    'unit' => '',
    'exponent' => 0,
    'variable' => 7,
    'info' => 16,
  ),
  19 => 
  array (
    'mark' => 'rez2 min',
    'unit' => '',
    'exponent' => 0,
    'variable' => 7,
    'info' => 16,
  ),
  20 => 
  array (
    'mark' => 'rez2 max',
    'unit' => '',
    'exponent' => 0,
    'variable' => 7,
    'info' => 16,
  ),
  21 => 
  array (
    'mark' => 'waga imp',
    'unit' => 'm3/impuls',
    'exponent' => 0,
    'variable' => 7,
    'info' => 16,
  ),
  22 => 
  array (
    'mark' => 'średnica otworu kryzy',
    'unit' => 'mm',
    'exponent' => 0,
    'variable' => 7,
    'info' => 16,
  ),
  23 => 
  array (
    'mark' => 'średnica rurociągu',
    'unit' => 'mm',
    'exponent' => 0,
    'variable' => 7,
    'info' => 16,
  ),
  24 => 
  array (
    'mark' => 'współczynnik rozszerzalności kryzy',
    'unit' => '',
    'exponent' => 0,
    'variable' => 7,
    'info' => 16,
  ),
  25 => 
  array (
    'mark' => 'współczynnik rozszerzalności rurociągu',
    'unit' => '',
    'exponent' => 0,
    'variable' => 7,
    'info' => 16,
  ),
  26 => 
  array (
    'mark' => 'metan',
    'unit' => '%',
    'exponent' => 0,
    'variable' => 7,
    'info' => 16,
  ),
  27 => 
  array (
    'mark' => 'etan',
    'unit' => '%',
    'exponent' => 0,
    'variable' => 7,
    'info' => 16,
  ),
  28 => 
  array (
    'mark' => 'propan',
    'unit' => '%',
    'exponent' => 0,
    'variable' => 7,
    'info' => 16,
  ),
  29 => 
  array (
    'mark' => 'n-butan',
    'unit' => '%',
    'exponent' => 0,
    'variable' => 7,
    'info' => 16,
  ),
  30 => 
  array (
    'mark' => 'i-butan',
    'unit' => '%',
    'exponent' => 0,
    'variable' => 7,
    'info' => 16,
  ),
  31 => 
  array (
    'mark' => 'n-pentan',
    'unit' => '%',
    'exponent' => 0,
    'variable' => 7,
    'info' => 16,
  ),
  32 => 
  array (
    'mark' => 'i-pentan',
    'unit' => '%',
    'exponent' => 0,
    'variable' => 7,
    'info' => 16,
  ),
  33 => 
  array (
    'mark' => 'n-hexan',
    'unit' => '%',
    'exponent' => 0,
    'variable' => 7,
    'info' => 16,
  ),
  34 => 
  array (
    'mark' => 'wodor',
    'unit' => '%',
    'exponent' => 0,
    'variable' => 7,
    'info' => 16,
  ),
  35 => 
  array (
    'mark' => 'rezerwa',
    'unit' => '',
    'exponent' => 0,
    'variable' => 7,
    'info' => 16,
  ),
  36 => 
  array (
    'mark' => 'hel',
    'unit' => '%',
    'exponent' => 0,
    'variable' => 7,
    'info' => 16,
  ),
  37 => 
  array (
    'mark' => 'azot',
    'unit' => '%',
    'exponent' => 0,
    'variable' => 7,
    'info' => 16,
  ),
  38 => 
  array (
    'mark' => 'tlen',
    'unit' => '%',
    'exponent' => 0,
    'variable' => 7,
    'info' => 16,
  ),
  39 => 
  array (
    'mark' => 'dwutlenek węgla',
    'unit' => '',
    'exponent' => 0,
    'variable' => 7,
    'info' => 16,
  ),
  40 => 
  array (
    'mark' => 'współczynnik korekcyjny',
    'unit' => '',
    'exponent' => 0,
    'variable' => 7,
    'info' => 8,
  ),
  41 => 
  array (
    'mark' => 'współczynnik ściśliwości',
    'unit' => '',
    'exponent' => 0,
    'variable' => 7,
    'info' => 8,
  ),
  42 => 
  array (
    'mark' => 'liczba Reynoldsa',
    'unit' => '',
    'exponent' => 0,
    'variable' => 7,
    'info' => 8,
  ),
  43 => 
  array (
    'mark' => 'gęstość gazu w warunkach pomiaru',
    'unit' => '',
    'exponent' => 0,
    'variable' => 7,
    'info' => 8,
  ),
  44 =>
  array (
    'mark' => '?',
    'unit' => '',
    'exponent' => 0,
    'variable' => 7,
    'info' => 8,
  ),
);