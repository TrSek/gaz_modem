<?php
define ('GM_READ_IDENTIF_NULL',    0x00);   // cteni identifikacnich parametru zarizeni
define ('GM_READ_DP_DEF',          0x01);   // cteni pole dostupnych parametru DP
define ('GM_READ_EVENT_DEF',       0x02);   // cteni definicie alarmu a eventu
define ('GM_CURRENT_DATA',         0x0A);   // cteni aktualnich dat
define ('GM_READ_DP_DEF_FULL',     0x20);   // cteni podrobneho popisu parametru DP
define ('GM_READ_ZD_DEF',          0x03);   // cteni definice tabulky udalosti ZD
define ('GM_READ_KWDB_DEF',        0x05);   // cteni poradi v tabulce v KWDB
define ('GM_WRITE_KWDB_DEF',       0x06);   // modifikace poradi posilanych dat v tabulce KWDB
define ('GM_READ_IDENTIF_SHORT',   0x09);   // cteni identifikacnich parametru zarizeni
define ('GM_READ_IDENTIF',         0x27);   // cteni statusu zarizeni
define ('GM_READ_KWDB_ACTUAL',     0x40);   // cteni aktualnich dat podle tabulky KWDB
define ('GM_SEARCH_HOURLY_1',      0x1A);   // vyhledavani v hodinovem archivu GM1
define ('GM_SEARCH_HOURLY',        0x41);   // vyhledavani v hodinovem archivu
define ('GM_SEARCH_DAILY',         0x21);   // vyhledavani v dennim archivu
define ('GM_SEARCH_DAILY_1',       0x1B);   // vyhledavani v dennim archivu GM1
define ('GM_DAILY_FROM_INDEX',     0x22);   // vycteni denniho archivu od indexu
define ('GM_DAILY_INDEX_1',        0x1E);   // vycteni vety denniho archivu v indexu GM1
define ('GM_DAILY_FROM_INDEX_1',   0x1F);   // vycteni denniho archivu od indexu GM1
define ('GM_HOURLY_FROM_INDEX',    0x42);   // vycteni hodinoveho archivu od indexu
define ('GM_HOURLY_INDEX_1',       0x1C);   // vycteni vety hodinoveho archivu v indexu GM1
define ('GM_HOURLY_FROM_INDEX_1',  0x1D);   // vycteni hodinoveho archivu od indexu GM1
define ('GM_READ_ALARM',           0x3A);   // vycteni alarmu podle indexu
define ('GM_WRITE_PARAM',          0x44);   // modifikace parametru
define ('GM_WRITE_PARAM_AUTH',     0x29);   // modifikace parametru + authorizacne data
define ('GM_READ_ZD',              0x46);   // cteni seznamu udalosti
define ('GM_READ_ZD_INDEX',        0x47);   // cteni seznamu udalosti podle indexu
define ('GM_READ_TIME',            0x48);   // cteni casu
define ('GM_WRITE_TIME',           0x49);   // nastaveni casu
define ('GM_READ_TIME_1',          0x4B);   // cteni casu
define ('GM_WRITE_TIME_1',         0x4A);   // nastaveni casu
define ('GM_SYNCHRON_TIME',        0x26);   // jemne doladeni hodin
define ('GM_SYNCHRON_TIME_MEAS',   0x2A);   // synchronizace pristroje v podminkach mereni
define ('GM_READ_STATUS',          0x2B);   // cteni okamzitych stavovych bitu
define ('GM_AUTHORIZE',            0x4F);   // zaslani uzivatelskeho jmena/hesla
define ('GM_READ_DP_DEF_FULL2',    0x50);   // cteni parametru pole DP s uplnym popisem, rozsahem odchylky a docasne registracni znacky
define ('GM_READ_ACTUAL',          0x43);   // cteni dat bez pouziti KWDB
define ('GM_WRITE_ACTUAL',         0x45);   // modifikace dat bez pouziti KWDB
define ('GM_WRITE_TIME_AUTH',      0x51);   // nastaveni casu + authorizacne data
define ('GM_READ_SYNCHRON',        0x52);   // pocitadlo synchronizace v ramci mereni
define ('GM_ASK_AUTHORIZE',        0xB0);   // vyzadovany autorizacny packet
define ('GM_MULTI_READ',           0x32);   // MultiDotaz
define ('GM_READ_INFO',            0x68);   // Konfigurace prepocitavace
define ('GM_REPEAT_COMMAND',       0x7D);   // repeat command
define ('GM_ERROR_DATA',           0x7E);   // data error
define ('GM_ERROR_COMMAND',        0x7F);   // command error

define ('GM_READ_CONTINUE',        0xE5);   // pokracovani v zapocatem prenosu
define ('GM_ELGAS_TUNEL',          0xFF);   // elgas tunel

define ('GM_FUNCT_DESC',  0);
define ('GM_FUNCT_NEED',  1);
define ('GM_ANSWER_BITE',  0x80);

$funct_code = 
	array(
			GM_READ_IDENTIF_NULL    => array('Device identification parameters readout'),
			GM_READ_DP_DEF          => array('Available parameters (DP) array readout'),
			GM_READ_EVENT_DEF       => array('Alarms definition and events array readout'),
			GM_CURRENT_DATA         => array('Current data readout'),
			GM_READ_DP_DEF_FULL     => array('Read DP'),
			GM_READ_ZD_DEF          => array('Read ZD define'),
			GM_READ_KWDB_DEF        => array('Read KWDB define'),
			GM_WRITE_KWDB_DEF       => array('Modify KWDB define'),
			GM_READ_IDENTIF_SHORT   => array('Read identify device short'),
			GM_READ_IDENTIF         => array('Read identify device'),
			GM_READ_KWDB_ACTUAL     => array('Read KWDB'),
			GM_SEARCH_HOURLY        => array('Search in period archive'),
			GM_SEARCH_HOURLY_1      => array('Search in period archive GM1'),
			GM_SEARCH_DAILY         => array('Search in daily archive'),
			GM_SEARCH_DAILY_1       => array('Search in daily archive GM1'),
			GM_HOURLY_FROM_INDEX    => array('Read period archive from timestamp'),
			GM_HOURLY_INDEX_1       => array('Read archive stored in timestamp GM1'),
			GM_HOURLY_FROM_INDEX_1  => array('Read period archive from timestamp GM1'),
			GM_DAILY_FROM_INDEX     => array('Read daily archive in timestamp'),
			GM_DAILY_INDEX_1        => array('Read daily stored in timestamp GM1'),
			GM_DAILY_FROM_INDEX_1   => array('Read daily archive from timestamp GM1'),
			GM_READ_ALARM           => array('Read alarms'),					
			GM_WRITE_PARAM          => array('Modify parameters'),
			GM_WRITE_PARAM_AUTH     => array('Modify parameters with authorize'),
			GM_READ_ZD              => array('Read ZD'),
			GM_READ_ZD_INDEX        => array('Read ZD by index'),
			GM_READ_TIME            => array('Read time'),
			GM_WRITE_TIME           => array('Write time'),
			GM_READ_TIME_1          => array('Read time GM1'),
			GM_WRITE_TIME_1         => array('Write time GM1 (without answer)'),
			GM_SYNCHRON_TIME        => array('Synchronize time'),
			GM_SYNCHRON_TIME_MEAS   => array('Synchronize time by measurement'),
			GM_READ_STATUS          => array('Read status'),
			GM_AUTHORIZE            => array('Authorize'),
			GM_READ_DP_DEF_FULL2    => array('Read full DP table2'),
			GM_READ_ACTUAL          => array('Read actual data'),
			GM_WRITE_ACTUAL         => array('Modify actual data'),
			GM_WRITE_TIME_AUTH      => array('Set time with authorize'),
			GM_READ_SYNCHRON        => array('Read synchronize frame'),
			GM_ASK_AUTHORIZE        => array('Ask for authorize'),
			GM_MULTI_READ           => array('MultiRead'),
			GM_ERROR_DATA           => array('Data error'),
			GM_ERROR_COMMAND        => array('Command error'),
			GM_READ_INFO            => array('Read Info'),
			GM_REPEAT_COMMAND       => array('Repeat last command'),
			GM_READ_CONTINUE        => array('Continue read'),
			GM_ELGAS_TUNEL          => array('Elgas tunel'),
	);
	

/********************************************************************
* @brief Human information about Function
* @param $funct_text - identification in dec format
*/
function GM_funct_name($funct_text, &$gm_funct_id, &$is_answer)
{
	global $funct_code;
	$answer = [];
	
	// je to odpoved
	if(( $funct_code[$funct_text]==null )
	&& ( $funct_text >= GM_ANSWER_BITE ))
	{
		$gm_funct_id = $funct_text & ~GM_ANSWER_BITE;
		$is_answer = true;
	}
	else
	{
		$gm_funct_id = $funct_text;
		$is_answer = false;
	}
	
	if(( $gm_funct_id == GM_ERROR_DATA )
	|| ( $gm_funct_id == GM_ERROR_COMMAND ))
		$is_answer = true;
		
	if( $funct_code[$gm_funct_id]==null )
		$answer[] = strtoupper(dechex($gm_funct_id)). "h - unknown";
	else
		$answer[] = strtoupper(dechex($gm_funct_id)). "h - ". $funct_code[$gm_funct_id][GM_FUNCT_DESC];
		
	if( $is_answer )
		$answer[] = dechex(GM_ANSWER_BITE) ."h - Answer packet";
	
	return $answer;
}

/*----------------------------------------------------------------------------*/
/* END OF FILE */
