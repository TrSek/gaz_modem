<?php
require_once("obj/objects.inc");

/********************************************************************
 * @brief Rot hex to other endian
 */
function rotOrder($cut_str, $len)
{
	$answer = "";
	while( $len-- )
	{
		$answer .= substr($cut_str, 2*$len, 2);
		$cut_str = substr($cut_str, 0, 2*$len);
	}

	return $answer;
}

/********************************************************************
* @brief Strip 'len' chars from start of string 
*/
function substr_cut(&$SMS, $len)
{
	$cut_str = substr($SMS, 0, 2*$len);
	$SMS = substr($SMS, 2*$len, strlen($SMS) - 2*$len);
	return $cut_str;
}

/********************************************************************
* @brief Najde retazec ale na parnej pozicii
*/
function substr_pos($SMS, $find_str)
{
	$len = strlen($SMS);
	$poz = 0;
	
	while( (substr($SMS, $poz, 2) != $find_str)
		&& ($poz <= $len))
	{
		$poz += 2;		
	}
	
	if( $poz > $len )
		$poz = 0;
	
	return $poz;
}

/********************************************************************
* @brief hex2bin in PHP < 5.4.0
*/
if (PHP_VERSION_ID < 50400) {
function hex2bin($hex_string)
{
	return pack("H*" , $hex_string);
}
}

/********************************************************************
* @brief Convert hex to string
*/
function hexToStr($hex)
{
	$string='';
	for ($i=0; $i < strlen($hex)-1; $i+=2){
		$string .= chr(hexdec($hex[$i].$hex[$i+1]));
	}
	return $string;
}

/********************************************************************
* @brief Return text presentation unit of measure
*/
function gm_get_mj($unit)
{
	$unit_str = array(
		 UNIT_DEFAULT => "",			
		 UNIT_stC     => "°C",
		 UNIT_stF     => "°F",
		 UNIT_Kelvin  => "K",
		 UNIT_stR     => "°R",
		 UNIT_kPa     => "kPa",
		 UNIT_Pa      => "Pa",
		 UNIT_MPa     => "MPa",
		 UNIT_bar     => "bar",
		 UNIT_torr    => "torr",
		 UNIT_PSI     => "PSI",
		 UNIT_at      => "at",
		 UNIT_kgfcm2  => "kgf/cm²",
		 UNIT_atm     => "atm",
		 UNIT_MJm3    => "MJ/m³",
		 UNIT_kWhm3   => "kWh/m³",
		 UNIT_Btuft3  => "Btu/ft³",
		 UNIT_MJ      => "MJ",
		 UNIT_kWh     => "kWh",
		 UNIT_Btu     => "Btu",
		 UNIT_m3      => "m³",
		 UNIT_ft3     => "ft³",
		 UNIT_nm3     => "nm³",
		 UNIT_nft3    => "nft³",
		 UNIT_m3h     => "m³/h",
		 UNIT_ft3h    => "ft³/h",
		 UNIT_nm3h    => "nm³/h",
		 UNIT_nft3h   => "nft³/h",
		 UNIT_yard3h  => "yard³/h",
		 UNIT_galh    => "gal³/h",
		 UNIT_yard3   => "yard³",
		 UNIT_gal     => "galon",
		 UNIT_V       => "V",
		 UNIT_mV      => "mV",
		 UNIT_A       => "A",
		 UNIT_mA      => "mA",
		 UNIT_m       => "m",
		 UNIT_ft      => "ft",
		 UNIT_kgm3    => "kg/m³",
		 UNIT_h       => "hrs",
		 UNIT_dB      => "dB",
		 UNIT_HEX     => "h",
		 UNIT_percent => "%",
	);
		
	return $unit_str[$unit];
}

/********************************************************************
 * @brief Je to chybova odpoved?
 */
function isError($error)
{
	if(( strlen($error) == 2 )
    && (( $error == "7F")
     || ( $error == "7E")
     || ( $error == "7D")
     || ( $error == "7C"))
	) {
		return true;
	}
	
	return false;
}

/********************************************************************
 * @brief Vrati popis chyby
 */
function gm_get_error($error)
{
	$error_str = array(
		0x7F => "Attempt to read non-existent data",
		0x7E => "Unknown command or invalid length",
		0x7D => "Authorize error",
		0x7C => "Command is correct but data is invalid",
	);
	
	$answer = $error ." - ". $error_str[$error];
	return $answer;
}

/********************************************************************
 * @brief Vrati popis statusu
 */
function gm_get_status($status)
{
	$status_str = array(
			0x01 => "Parameters from measurement",
			0x02 => "Setpoint error - process alarm",
			0x04 => "Setpoint error - system alarm",
			0x08 => "Computable value",
			0x10 => "Constant value",
			0x20 => "Hourly correction",
//			0x40 => "",
			0x80 => "Discontinuity",
	);
	
	$answer = [];
	foreach ($status_str as $mask => $value)
	{
		if( $status & $mask )
			$answer[] = dechex($mask) ."h - ". $value;
	}
	
	return $answer;
}

/********************************************************************
* @brief date presentation
*/
function gm_date($DATI, $type)
{
	$data_msecond = ($type >= eGM_DATETIME_FULL)?   hexdec(substr_cut($DATI, 1)): 0;
	$data_second  = ($type >= eGM_DATETIME)?        hexdec(substr_cut($DATI, 1)): 0;
	$data_minute  = ($type >= eGM_DATETIME_HOURLY)? hexdec(substr_cut($DATI, 1)): 0;
	$data_hour    = ($type >= eGM_DATETIME_DAILY)?  hexdec(substr_cut($DATI, 1)): 0;
	$data_day     = hexdec(substr_cut($DATI, 1));
	$data_month   = hexdec(substr_cut($DATI, 1));
	$data_year    = hexdec(substr_cut($DATI, 1))+2000;

	switch($type)
	{
		case eGM_DATETIME_DAILY:
			return sprintf("%04d-%02d-%02d %02d:00:00", $data_year, $data_month, $data_day, $data_hour);
		case eGM_DATETIME_HOURLY:
			return sprintf("%04d-%02d-%02d %02d:%02d:00", $data_year, $data_month, $data_day, $data_hour, $data_minute);
		case eGM_DATETIME:
			return sprintf("%04d-%02d-%02d %02d:%02d:%02d", $data_year, $data_month, $data_day, $data_hour, $data_minute, $data_second);
		case eGM_DATETIME_FULL:
			return sprintf("%04d-%02d-%02d %02d:%02d:%02d.%02d", $data_year, $data_month, $data_day, $data_hour, $data_minute, $data_second, $data_msecond);
	}
	return "";
}

/********************************************************************
 * @brief date presentation
 */
function gm_TimeStamp($DATI)
{
	$dtime = DateTime::createFromFormat("Y-m-d H:i:s", $DATI);
	$timestamp = empty($dtime)? 0: $dtime->getTimestamp();
	return $timestamp;
}

/********************************************************************
* @brief When need value to one line
*/
function array_val_line($value)
{
	if(!is_array($value))
		return $value[0];

	$answer = "";
	foreach ($value[0] as $value_line)
	{
		$answer .= (empty($answer)? "": ", "). $value_line;
	}
	return $answer;
}

/********************************************************************
* @brief Parse value
* @type = 
*	eGM_UNDEF
*	eGM_SHORT_INT   - -128 .. +127
*	eGM_INT  		- -32768 .. +32767
*	eGM_LONG_INT	- -2147483648 .. +2147483647
*	eGM_BYTE		- 0..255
*	eGM_WORD		- 0.. 65535
*	eGM_DWORD		- 0.. 4294967295
*	eGM_SHORT_REAL	- 3,4E-38 .. 3,4E+38 - float
*	eGM_LONG_REAL	- 1,7E-308 .. 1,7E+308 - double
*	eGM_BOOLEAN		- 0,1
*	eGM_BCD			- cele cislo bez znamenka, koncova znacka je bajt s hodnotou 0xFF
*	eGM_STRING
*	eGM_INT_64		- -9223372036854775808..+9223372036854775807
*	eGM_UINT_64		- 0 .. 18446744073709551615
*/
function gm_val_size($type, $DATI)
{
	switch( $type )
	{
		case eGM_SHORT_INT:
		case eGM_BYTE:
		case eGM_BOOLEAN:
			$len = 1;
			break;
		case eGM_INT:
		case eGM_WORD:
			$len = 2;
			break;
		case eGM_LONG_INT:
		case eGM_SHORT_REAL:
		case eGM_DWORD:
			$len = 4;
			break;
		case eGM_MIDDLE_REAL:
			$len = 6;
			break;
		case eGM_LONG_REAL:
		case eGM_INT_64:
		case eGM_UINT_64:
			$len = 8;
			break;
		case eGM_BCD:
			$len = (substr_pos($DATI, "FF")/2) + 1;
			break;
		case eGM_STRING:
			$len = (substr_pos($DATI, "00")/2) + 1;
			break;
		case eGM_DATETIME_DAILY:
			$len = 4;
			break;
		case eGM_DATETIME_HOURLY:
			$len = 5;
			break;
		case eGM_DATETIME:
			$len = 6;
			break;
		case eGM_DATETIME_FULL:
			$len = 7;
			break;					
		case eGM_UNDEF:
		default:
			$len = 0;
			break;
	}
	
	return $len;
}

function hex2float($strHex)
{
	$hex = sscanf($strHex, "%02x%02x%02x%02x");
	if( !is_array($hex))
		return 0;
	$hex = array_reverse($hex);
	$bin = implode('', array_map('chr', $hex));
	$array = unpack("f", $bin);
	return $array[1];
}

function hex2double($strHex)
{
	$hex = sscanf($strHex, "%02x%02x%02x%02x%02x%02x%02x%02x");
	if( !is_array($hex))
		return 0;
	$hex = array_reverse($hex);
	$bin = implode('', array_map('chr', $hex));
	$array = unpack("dnum", $bin);
	return $array['num'];
}

function real48Todouble($value)
{
  $mantissa = 1.0;
  $real48 = pack("H*" , $value);
  for ($i = 46; $i >= 8; $i--)
  {
  	if ((ord($real48[$i / 8]) >> ($i % 8)) & 0x01) 
    {
      $mantissa += pow(2.0, $i - 47);
    }
  }

  $exponent = ord($real48[0]) - 129;

  if (($mantissa == 1.0) && ($exponent == -129)) // Test for null value
    return 0.0;

  if (ord($real48[5]) & 0x80) // Sign bit check
    $mantissa *= -1.0;

  return $mantissa * pow(2.0, $exponent);
}

function doubleToReal48($double)
{
	$byteArray = array_values( unpack('C*', pack('d', $double)) ); // 64 bit double as array of integers
	$real48 = array(0, 0, 0, 0, 0, 0);

	// Copy the negative flag
	$real48[5] |= ($byteArray[7] & 128);

	// Get the exponent
	$n = ($byteArray[7] & 127) << 4;
	$n |= ($byteArray[6] & 240) >> 4;

	if ($n == 0) { // Zero exponent = 0
		return pack('c6', $real48[0], $real48[1], $real48[2], $real48[3], $real48[4], $real48[5]);
	}

	$real48[0] = $n - 1023 + 129;

	// Copy the Mantissa
	$real48[5] |= (($byteArray[6] & 15) << 3); // Get the last 4 bits
	$real48[5] |= (($byteArray[5] & 224) >> 5); // Get the first 3 bits
	for ($b = 4; $b >= 1; $b--) {
		$real48[$b] = (($byteArray[$b+1] & 31) << 3); // Get the last 5 bits
		$real48[$b] |= (($byteArray[$b] & 224) >> 5); // Get the first 3 bits
	}

	return pack('c6', $real48[0], $real48[1], $real48[2], $real48[3], $real48[4], $real48[5]);
}

function gm_val(&$DATI, $type)
{
	$len = gm_val_size($type, $DATI);
	$value = substr_cut($DATI, $len);

	switch( $type )
	{
		case eGM_SHORT_INT:
		case eGM_INT:
		case eGM_LONG_INT:
		case eGM_BYTE:
		case eGM_WORD:
		case eGM_DWORD:
		case eGM_BOOLEAN:
		case eGM_INT_64:
		case eGM_UINT_64:
		case eGM_BCD:
				$value = rotOrder($value, $len);
				$value = hexdec($value);
				break;
		case eGM_STRING:
			    $value = trim(iconv("Windows-1252", "UTF-8", hexToStr($value)));
				break;
		case eGM_SHORT_REAL:
				$value = rotOrder($value, $len);
				$value = hex2float($value);
				break;
		case eGM_MIDDLE_REAL:
				$value = real48Todouble($value);
				break;
		case eGM_LONG_REAL:
				$value = rotOrder($value, $len);
				$value = hex2double($value);
				break;
		case eGM_DATETIME_FULL:		// milisekunda, sekunda, minuta, hodina, den, mesiac, rok
		case eGM_DATETIME:			// sekunda, minuta, hodina, den, mesiac, rok
		case eGM_DATETIME_HOURLY:	// minuta, hodina, den, mesiac, rok
		case eGM_DATETIME_DAILY:	// hodina, den, mesiac, rok
				$value = gm_date($value, $type);
				break;
		case eGM_UNDEF:
				break;
	}
	
	return $value;
}
