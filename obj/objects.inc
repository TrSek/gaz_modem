<?php
define ('DP_TABLE_FILE', './log/dp_table.php');
define ('DP_TABLE_FILE1', './log/dp_table1.php');
define ('DP_TABLE_FILE2', './log/dp_table2.php');
define ('ZD_TABLE_FILE', './log/zd_table.php');
define ('KWDB_TABLE_FILE', './log/kwdb_table.php');
define ('UNPACK_LOG_FILE', './log/unpack_log_packet.txt');

define ('MAX_HTML_ROWS',      12);
define ('GM_COMPANION_OBJ_ID', 0);
define ('GM_TYPE',             1);
define ('GM_UNIT',             2);
define ('GM_LEN',              3);
define ('GM_DESCRIPTION',      4);
define ('GM_BROADCAST',   'FFFF');
define ('SEC_PER_MINUTE',     60);

define ('eGM_UNDEF',           0);
define ('eGM_SHORT_INT',       1);	// -128 .. +127
define ('eGM_INT',             2);	//  -32768 .. +32767
define ('eGM_LONG_INT',        3);	// -2147483648 .. +2147483647
define ('eGM_BYTE',            4);	// 0..255
define ('eGM_WORD',            5);	// 0.. 65535
define ('eGM_DWORD',           6);	// 0.. 4294967295
define ('eGM_SHORT_REAL',      7);	// 3,4E-38 .. 3,4E+38 - float
define ('eGM_LONG_REAL',       9);	// 1,7E-308 .. 1,7E+308 - double
define ('eGM_BOOLEAN',        10);	// 0,1
define ('eGM_BCD',            11);	// cele cislo bez znamenka, koncova znacka je bajt s hodnotou 0xFF
define ('eGM_STRING',         12);
define ('eGM_INT_64',         13);	// -9223372036854775808..+9223372036854775807
define ('eGM_UINT_64',        14);	// 0 .. 18446744073709551615

define ('eGM_MIDDLE_REAL',     89);	// pascal format
define ('eGM_DATETIME_DAILY',  90);	// hodina, den, mesiac, rok
define ('eGM_DATETIME_HOURLY', 91);	// minuta, hodina, den, mesiac, rok
define ('eGM_DATETIME',		   92);	// sekunda, minuta, hodina, den, mesiac, rok
define ('eGM_DATETIME_FULL',   93);	// milisekunda, sekunda, minuta, hodina, den, mesiac, rok

$GM_VARIABLE_STR = array
(
	eGM_UNDEF		=> "undef",
	eGM_SHORT_INT	=> "short int",
	eGM_INT     	=> "int",
	eGM_LONG_INT	=> "long int",
	eGM_BYTE		=> "byte",
	eGM_WORD		=> "word",
	eGM_DWORD		=> "double word",
	eGM_SHORT_REAL	=> "short real",
	eGM_LONG_REAL	=> "long real",
	eGM_BOOLEAN		=> "boolean",
	eGM_BCD			=> "bcd",
	eGM_STRING		=> "string",
	eGM_INT_64		=> "int64",
	eGM_UINT_64		=> "uint64",
);

/*----------------------------------------------------------------------------*/
/* END OF FILE */
