<?php
require_once("obj/objects.php");

// for GAZ-MODEM3
function gm_load_DP()
{
	// TODO prerobit
	if( file_exists(DP_TABLE_FILE)) {
		require(DP_TABLE_FILE);
	}
	return $dp_table;
}

// for GAZ-MODEM1
function gm_load_DP1()
{
	if( file_exists(DP_TABLE_FILE1)) {
		require(DP_TABLE_FILE1);
	}
	return $dp_table;
}

// for GAZ-MODEM2
function gm_load_DP2()
{
	if( file_exists(DP_TABLE_FILE2)) {
		require(DP_TABLE_FILE2);
	}
	return $dp_table;
}

function gm_save_DP($dp_table)
{
	global $is_crc_correct;

	if( $is_crc_correct ) {
		file_put_contents(DP_TABLE_FILE, '<?php $dp_table = ' . var_export($dp_table, true) . ';');
	}
}

function gm_DP_Info($dp_index)
{
	$dp_table = gm_load_DP();
	$answer = $dp_index ." - ". $dp_table[$dp_index]['mark'] ." (". $dp_table[$dp_index]['unit'] .")";
	return $answer;
}

function gm_load_KWDB()
{
	// TODO prerobit
	if( file_exists(KWDB_TABLE_FILE)) {
		require(KWDB_TABLE_FILE);
	}
	else {
		// vynulovat
		$maxs = 0;
		while( $maxs < 1024 )
		{
			$kwdb_table[$maxs] = $maxs;
			$maxs++;
		}
	}
	return $kwdb_table;
}

function gm_save_KWDB($kwdb_table)
{
	global $is_crc_correct;
	$maxs = (count($kwdb_table) > 0)? 
			max(array_keys($kwdb_table)): 0;
	
	if( !is_numeric($maxs))
		$maxs = 0;

	// doplnit chybajuce
	while( $maxs < 1024 )
	{
		$kwdb_table[$maxs] = $maxs;
		$maxs++;
	}

	if( $is_crc_correct ) {
		file_put_contents(KWDB_TABLE_FILE, '<?php $kwdb_table = ' . var_export($kwdb_table, true) . ';');
	}
}

function gm_load_ZD()
{
	// TODO prerobit
	if( file_exists(ZD_TABLE_FILE)) {
		require(ZD_TABLE_FILE);
	}
	return $zd_table;
}

function gm_save_ZD($zd_table)
{
	global $is_crc_correct;

	if( $is_crc_correct) {
		return file_put_contents(ZD_TABLE_FILE, '<?php $zd_table = ' . var_export($zd_table, true) . ';');
	}
}

function gm_ZD_Info($zd_index)
{
	$zd_table = gm_load_ZD();
	$answer = $zd_index ." - ". $zd_table[$zd_index]['mark'];
	return $answer;
}

function gm_DP1_arch()
{
	$dp_table = array (
			0 => array ( 'mark' => 'dVb', 'unit' => 'm3',  'variable' => eGM_SHORT_REAL ),
			1 => array ( 'mark' => 'dVm', 'unit' => 'm3',  'variable' => eGM_SHORT_REAL ),
			2 => array ( 'mark' => 'P',   'unit' => 'kPa', 'variable' => eGM_SHORT_REAL ),
			3 => array ( 'mark' => 't',   'unit' => '°C',  'variable' => eGM_SHORT_REAL ),
			4 => array ( 'mark' => 'dP',  'unit' => 'kPa', 'variable' => eGM_SHORT_REAL )
	);
	return $dp_table;
}

function gm_DP1_arch_Info($dp_index)
{
	$dp_table = gm_DP1_arch();
	
	if( !empty($dp_table[$dp_index]))
		$answer = $dp_index ." - ". $dp_table[$dp_index]['mark'] ." (". $dp_table[$dp_index]['unit'] .")";
	
	return $answer;
}
