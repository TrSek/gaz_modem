<?php
require_once("crc/crc.php");
require_once("obj/objects.inc");
require_once("funct/funct_name.php");
require_once("obj/objects.php");

/********************************************************************
* @brief Remove 0A/0D if have it.
*/
function GM_NORMALIZE($FRAME)
{
	$FRAME = strtoupper($FRAME);
	
	// strip all spaces
	$FRAME = str_replace(' ', '', $FRAME);
	$FRAME = str_replace("\r", '', $FRAME);
	$FRAME = str_replace("\n\n", '\n', $FRAME);
	
	// glued packet
	$FRAME_OUT = "";
	foreach (explode("1668", $FRAME) as $FRAME_LINE)
	{
		if( strlen($FRAME_LINE))
			$FRAME_OUT .= (strlen($FRAME_OUT)? "16\n68": "") .$FRAME_LINE;
	}
	
	// check start-stop bajt
	$FRAME = $FRAME_OUT;
	$FRAME_OUT = "";
	foreach (explode("\n", $FRAME) as $FRAME_LINE)
	{
		if( !strlen($FRAME_LINE)
		|| ($FRAME_LINE == "00"))
			continue;
		
		// when not started with 0x68 - we are find start
		if(( $poz = strpos($FRAME_LINE, "68")) > 0 )
		{
             $FRAME_LINE = substr($FRAME_LINE, 0, $poz) ."\n". substr($FRAME_LINE, $poz, strlen($FRAME_LINE) - $poz);
		} 

		// when not stoped with 0x16 - split
		if( substr($FRAME_LINE, -2) != "16" )
		if(( $poz = strrpos($FRAME_LINE, "16")) > 0 )
		{
			$poz += 2;
			$FRAME_LINE = substr($FRAME_LINE, 0, $poz) ."\n". substr($FRAME_LINE, $poz, strlen($FRAME_LINE) - $poz);
		}
		
		$FRAME_OUT .= (strlen($FRAME_OUT)? "\n": "") .$FRAME_LINE;
	}
	
	return $FRAME_OUT;
}

/********************************************************************
 * @brief How many rows need
 */
function gmRowsOptimal($FRAME)
{
	$rows = substr_count($FRAME, "\n")+1;
	if( $rows < MAX_HTML_ROWS )
	{
		// pri viac-riadkovom pakete
		foreach (explode("\n", $FRAME) as $FRAME_LINE)
		{
			$rows += round( (strlen($FRAME_LINE)/94) - 0.5, 0, PHP_ROUND_HALF_DOWN );
		}
	}
	
	$rows = min(MAX_HTML_ROWS, max(2, $rows));
	return $rows;
}

/********************************************************************
 * @brief Show disp information in short view
 */
function gmDisp($funct)
{
	$answer  = (count($funct)>1)? " >> ": " << ";
	$answer .= $funct[0];
	return $answer;
}

/********************************************************************
 * @brief Added space every $len
 */
function add_soft_space($DATI, $len)
{
	$answer = "";
	while( strlen($DATI))
	{	
		$answer .= substr($DATI, 0, $len) ." ";
		$DATI = substr($DATI, $len, strlen($DATI));
	}   

	return $answer;
}

/********************************************************************
* @brief Make HTML format from array
* @retval HTML format divide by <br>
*/
function gm_array_show($value)
{
	if( is_array($value) && count($value)==1)
		$value = $value[0];
	
	if( !is_array($value))
	{
		$space = "";
		while($value[0] == ' ')
		{
			$space .= "&nbsp;";
			$value = substr($value, 1, strlen($value));
		}
		return $space. $value;
	}

	$out = "";
	foreach ($value as $value_line)
	{
		$out .= gm_array_show($value_line) . "<br>";
	}
	return $out;
}

/********************************************************************
* @brief Show analyze FRAME
* @retval HTML table format
*/
function gm_show_packet($FRAME, &$disp)
{
	$GM_CRC = CRC16(substr($FRAME, 2, -6));
	$OUT_FRAME = gm_analyze_frame($FRAME, $GM_CRC);

	// poriadok s disp
	$disp = gmDisp($OUT_FRAME['F']);
	
	// bad CRC or length
	if(( strpos($OUT_FRAME['LEN'], "correctly"))
	|| ( strpos($OUT_FRAME['CRC'], "correctly"))) {
		$disp .= " *";
	}

	$out  = "<table class='table-style-two'>\n";
	foreach ($OUT_FRAME as $name => $value)
	{
		$out .= "<tr>";
		$out .= "<td>". $name ."</td>";
		$out .= "<td>";
		$out .= gm_array_show($value);
		$out .= "</td>";
		$out .= "</tr>";
	}
	$out .= "</table>";

	return $out;
}

/********************************************************************
* @brief Show analyze FRAME
* @retval HTML table format
*/
function gm_show($FRAME)
{
	$disp = "";
	$FRAME = explode("\n", $FRAME);

	// single line
	if( count($FRAME) <= 1)
		return gm_show_packet($FRAME[0], $disp);
	
	// multi line
	$first = true;
	foreach ($FRAME as $FRAME_LINE)
	{
		$hint = add_soft_space($FRAME_LINE, 24);
		$out_line = gm_show_packet($FRAME_LINE, $disp);
		$out .= "<li><a href='index.php?GM_FRAME=$FRAME_LINE' title='$hint'>+ $disp</a>";
		$out .= $first? "<ul class='hidden'>": "<ul>";
		$out .= $out_line;
		$out .= "<br></ul></li>";
		$first = false;
	}
	
	return "\n<ul class='menu'>". $out ."</ul>";
}

/********************************************************************
* @brief Check CRC
*/
function gm_CRCCheck($crc, $crc_compute)
{
	$crc_compute = substr($crc_compute, 2, 2). substr($crc_compute, 0, 2);
	
	if( $crc_compute == $crc )
		$answ = "$crc - OK";
	else
		$answ = "$crc - bad, correctly $crc_compute";
				
	return $answ;
}

/********************************************************************
* @brief MetaAnalyze frame name
*/
function gm_analyze_frame(&$FRAME, $GM_CRC)
{
	$real_len = strlen($FRAME)/2 - 6;
	$gm_funct = "";
	$is_answer = "";

	$FRAME_DATI = [];
	$FRAME_DATI['STX']  = substr_cut($FRAME, 1) ."h";
	$FRAME_DATI['LEN']  = ($len = hexdec(rotOrder( substr_cut($FRAME, 2), 2))) ." bytes";
	$FRAME_DATI['DA']   = rotOrder( substr_cut($FRAME, 2), 2) ."h";
	$FRAME_DATI['SA']   = rotOrder( substr_cut($FRAME, 2), 2) ."h";
	$FRAME_DATI['F']    = gm_funct_name( hexdec( substr_cut($FRAME, 1)), $gm_funct, $is_answer);
	$FRAME_DATI['DATI'] = substr_cut($FRAME, strlen($FRAME)/2 - 3);
	$FRAME_DATI['CRC']  = gm_CRCCheck(substr_cut($FRAME,2), $GM_CRC);
	$FRAME_DATI['ETX']  = substr_cut($FRAME, 1) ."h";
	
	$FRAME_DATI['DATI'] = gm_dati( $FRAME_DATI['DATI'], $gm_funct, $is_answer);
	// odstrani ak je prazdny
	if( empty($FRAME_DATI['DATI']))
		$FRAME_DATI['DATI'] = [];
	
	// bad length of packet
	if( $real_len != $len)
		$FRAME_DATI['LEN'] = "$len  bytes bad, correctly $real_len";

	// je Broadcast
	if( substr($FRAME_DATI['DA'], 0, 4) == GM_BROADCAST) $FRAME_DATI['DA'] .= " - broadcast"; 
	if( substr($FRAME_DATI['SA'], 0, 4) == GM_BROADCAST) $FRAME_DATI['SA'] .= " - broadcast"; 
		
	// kontroly
	if( $FRAME_DATI['STX'] != "68h" ) $FRAME_DATI['STX'] .= " - bad";
	if( $FRAME_DATI['ETX'] != "16h" ) $FRAME_DATI['ETX'] .= " - bad";
	
	return $FRAME_DATI;
}

/********************************************************************
* @brief Analyze data part
*/
function gm_dati($DATI, $gm_funct, $is_answer)
{
	// chybova odpoved
	if( isError($DATI))
		return gm_get_error($DATI);
	
	$answer = $DATI;
	switch ( $gm_funct )
	{
		case GM_READ_IDENTIF_SHORT:
		case GM_READ_IDENTIF_NULL:
			require_once 'struct/09-68-info.php';
			if( $is_answer )
				$answer = gm_info_short($DATI);
			break;
			
		case GM_READ_IDENTIF:
			require_once 'struct/09-68-info.php';
			if( $is_answer )
				$answer = gm_info($DATI);
			break;
			
		case GM_READ_INFO:
			require_once 'struct/09-68-info.php';
			if( $is_answer )
				$answer = gm_info_old2($DATI);
			break;
			
		case GM_AUTHORIZE:
			require_once 'struct/4F-authorize.php';
			$answer = gm_authorize_answer($DATI);
			break;

		case GM_READ_TIME:
		case GM_READ_TIME_1:
			require_once 'struct/48-49-date_time.php';
			if( $is_answer )
				$answer = gm_read_time($DATI);
			break;

		case GM_WRITE_TIME:
		case GM_WRITE_TIME_1:
			require_once 'struct/48-49-date_time.php';
			if( !$is_answer )
				$answer = gm_read_time($DATI);
			break;

		case GM_READ_ZD_DEF:
			require_once 'struct/03-47-table_ZD.php';
			if( !$is_answer )
				$answer = gm_read_zd_def($DATI);
			else
				$answer = gm_answer_zd_def($DATI);
			break;
			
		case GM_READ_DP_DEF:
			require_once 'struct/01-20-table_DP.php';
			if( !$is_answer )
				$answer = gm_read_dp_def($DATI);
			else
				$answer = gm_answer_dp_def($DATI);
			break;

		case GM_READ_DP_DEF_FULL:
			require_once 'struct/01-20-table_DP.php';
			if( !$is_answer )
				$answer = gm_read_dp_def($DATI);
			else
				$answer = gm_answer_dp_detail($DATI);
			break;

		case GM_READ_KWDB_DEF:
			require_once 'struct/05-06-40-table_KWDB.php';
			if( !$is_answer )
				$answer = gm_read_kwdb_def($DATI);
			else
				$answer = gm_answer_kwdb_def($DATI);
			break;
				
		case GM_WRITE_KWDB_DEF:
			require_once 'struct/05-06-40-table_KWDB.php';
			if( !$is_answer )
				$answer = gm_write_kwdb($DATI);
			break;
			
		case GM_READ_KWDB_ACTUAL:
			require_once 'struct/05-06-40-table_KWDB.php';
			if( !$is_answer )
				$answer = gm_ask_kwdb($DATI);
			else
				$answer = gm_read_kwdb($DATI);
			break;

		case GM_CURRENT_DATA:
			require_once 'struct/0A-table_current.php';
			if( !$is_answer )
				$answer = gm_ask_current($DATI);
			else
				$answer = gm_read_current($DATI);
			break;
				
		case GM_READ_ZD:
			require_once 'struct/03-47-table_ZD.php';
			if( !$is_answer )
				$answer = gm_ask_zd_index($DATI);
			else
				$answer = gm_read_zd($DATI);
			break;
					
		case GM_READ_ZD_INDEX:
			require_once 'struct/03-47-table_ZD.php';
			if( !$is_answer )
				$answer = gm_ask_zd_index($DATI);
			else
				$answer = gm_read_zd_index($DATI);
			break;

		case GM_SEARCH_HOURLY:
			require_once 'struct/1A-1B-1C-1D-1E-1F-21-22-41-42-archive.php';
			if( !$is_answer )
				$answer = gm_search_hourly($DATI);
			else
				$answer = gm_search_answer_hourly($DATI);
			break;

		case GM_SEARCH_HOURLY_1:
			require_once 'struct/1A-1B-1C-1D-1E-1F-21-22-41-42-archive.php';
			$answer = gm_search_hourly($DATI);
			break;

		case GM_SEARCH_DAILY:
			require_once 'struct/1A-1B-1C-1D-1E-1F-21-22-41-42-archive.php';
			if( !$is_answer )
				$answer = gm_search_daily($DATI);
			else
				$answer = gm_search_answer_daily($DATI);
			break;

		case GM_SEARCH_DAILY_1:
			require_once 'struct/1A-1B-1C-1D-1E-1F-21-22-41-42-archive.php';
			$answer = gm_search_daily($DATI);
			break;

		case GM_HOURLY_INDEX_1:
		case GM_DAILY_INDEX_1:
			require_once 'struct/1A-1B-1C-1D-1E-1F-21-22-41-42-archive.php';
			if( !$is_answer )
				$answer = gm_archiv_hourly_old($DATI);
			else
				$answer = gm_archiv_answer_hourly_old($DATI);
			break;
					
		case GM_HOURLY_FROM_INDEX_1:
			require_once 'struct/1A-1B-1C-1D-1E-1F-21-22-41-42-archive.php';
			if( !$is_answer )
				$answer = gm_archiv_hourly_old($DATI);
			else
				$answer = gm_archiv_answer_hourly_from_old($DATI);
			break;
					
		case GM_HOURLY_FROM_INDEX:
			require_once 'struct/1A-1B-1C-1D-1E-1F-21-22-41-42-archive.php';
			if( !$is_answer )
				$answer = gm_archiv_hourly($DATI);
			else
				$answer = gm_archiv_answer_hourly($DATI);
			break;
					
		case GM_DAILY_FROM_INDEX_1:
			require_once 'struct/1A-1B-1C-1D-1E-1F-21-22-41-42-archive.php';
			if( !$is_answer )
				$answer = gm_archiv_daily_old($DATI);
			else
				$answer = gm_archiv_answer_daily_old($DATI);
			break;
				
		case GM_DAILY_FROM_INDEX:
			require_once 'struct/1A-1B-1C-1D-1E-1F-21-22-41-42-archive.php';
			if( !$is_answer )
				$answer = gm_archiv_daily($DATI);
			else
				$answer = gm_archiv_answer_daily($DATI);
			break;
			
		case GM_READ_ALARM:
			require_once 'struct/3A-alarm.php';
			if( !$is_answer )
				$answer = gm_ask_alarm($DATI);
			else
				$answer = gm_read_alarm($DATI);
			break;
			
		case GM_WRITE_PARAM_AUTH:
			require_once 'struct/29-44-write.php';
			if( !$is_answer )
				$answer = gm_write_data_auth($DATI);
			break;
				
		case GM_WRITE_PARAM:
			require_once 'struct/29-44-write.php';
			if( !$is_answer )
				$answer = gm_write_data($DATI);
			break;
			
		case GM_ELGAS_TUNEL:
			$type   = hexdec(substr_cut($DATI, 1));
			$group  = hexdec(substr_cut($DATI, 1));
			$port   = hexdec(substr_cut($DATI, 1));
			$answer = json_decode( file_get_contents('http://'. $_SERVER['HTTP_HOST']. '/elgas2/index.php?JSON&ELGAS_FRAME='. $DATI. '&GROUP='. $group. '&TYPE='. $type), true);
			$DATI   = "";
			break;					
	}

	if( strlen($DATI))
	{
		if( is_array($answer))
			$answer[] = add_soft_space($DATI, 64);
		else
			$answer = add_soft_space($DATI, 64);
	}
	
	return $answer;
}
/*----------------------------------------------------------------------------*/
/* END OF FILE */
