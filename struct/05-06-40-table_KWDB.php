<?php
require_once("obj/objects.php");
require_once("obj/store_tables.php");

function gm_read_kwdb_def(&$DATI)
{
	$answer = [];
	$answer[] = gm_val($DATI, eGM_BYTE) ." - Count of DP";
	$answer[] = gm_val($DATI, eGM_WORD) ." - Index";
	return $answer;
}

function gm_answer_kwdb_def(&$DATI)
{
	$answer = [];
	$kwdb_table = gm_load_KWDB();

	$count = gm_val($DATI, eGM_BYTE);
	$index = gm_val($DATI, eGM_WORD);
	$answer[] = $count ." - Count of KWDB";
	$answer[] = $index ." - Index";
	$answer[] = "";
	$i = $index;

	while( $count-- )
	{
		$dp_index = gm_val($DATI, eGM_WORD);
		$kwdb_table[$i++] = $dp_index;
		$answer[] = gm_DP_Info( $dp_index );
	}

	gm_save_KWDB($kwdb_table);
	return $answer;
}

function gm_write_kwdb(&$DATI)
{
	$answer = [];
	$dp_table = gm_load_DP();
	$kwdb_table = gm_load_KWDB();

	$count    = gm_val($DATI, eGM_BYTE);
	$index_kwdb = gm_val($DATI, eGM_WORD);
	$answer[] = $count ." - Count of KWDB";
	$answer[] = $index_kwdb ." - Index";
	$answer[] = "";
	
	// resetuj tabulku KWDB
	if(($count == 0) && ($index_kwdb == 0))
	{
		$answer[] = "Reset table";
		$kwdb_table = [];
	}		
	
	while( $count-- )
	{
		$dp_index = gm_val($DATI, eGM_WORD);
		$answer[] = $index_kwdb ." = " . gm_DP_Info($dp_index);

		$kwdb_table[$index_kwdb] = $dp_index;
		$index_kwdb++;
	}
	
	gm_save_KWDB($kwdb_table);
	return $answer;
}

function gm_ask_kwdb(&$DATI)
{
	$answer = [];
	$dp_table = gm_load_DP();
	$kwdb_table = gm_load_KWDB();

	$count    = gm_val($DATI, eGM_BYTE);
	$index_kwdb = gm_val($DATI, eGM_WORD);
	$answer[] = $count ." - Count of KWDB";
	$answer[] = $index_kwdb ." - Index";
	$answer[] = "";
	
	while( $count-- )
	{
		$answer[] = $index_kwdb ." = " . gm_DP_Info($kwdb_table[$index_kwdb]);
		$index_kwdb++;
	}
	return $answer;
}

function gm_read_kwdb(&$DATI)
{
	$answer = [];
	$dp_table = gm_load_DP();
	$kwdb_table = gm_load_KWDB();

	$count    = gm_val($DATI, eGM_BYTE);
	$index_kwdb = gm_val($DATI, eGM_WORD);
	$answer[] = $count ." - Count of KWDB";
	$answer[] = $index_kwdb ." - Index";

	while( $count-- )
	{
		$dp_index = $kwdb_table[$index_kwdb++];
		$variable = $dp_table[$dp_index]['variable'];
		if( empty( $variable ))
			$variable = eGM_LONG;
		
		$status = gm_get_status( gm_val($DATI, eGM_BYTE));
		$value  = gm_val($DATI, $variable);

		$answer[] = "";
		$answer[] = $dp_table[$dp_index]['mark'] ." = ". $value ." ". $dp_table[$dp_index]['unit'];
		
		if( !empty($status))
		{
			foreach ($status as $status_line)
				$answer[] = "   -> ". $status_line;
		}
	}

	return $answer;
}

/*----------------------------------------------------------------------------*/
/* END OF FILE */
