<?php
require_once("obj/objects.php");
require_once("obj/store_tables.php");

function gm_write_data(&$DATI)
{
	$answer = [];
	$dp_table  = gm_load_DP();

	$count    = gm_val($DATI, eGM_BYTE);
	$dp_index = gm_val($DATI, eGM_WORD);
	$answer[] = $count ." - Count of Data";
	$answer[] = "";
	
	while($count--)
	{
		$variable = $dp_table[$dp_index]['variable'];
		$value    = gm_val($DATI, $variable);
		
		$answer[] = $dp_table[$dp_index]['mark'] ." = ". $value ." ". $dp_table[$dp_index]['unit'];
		$dp_index++;
	}
	
	return $answer;
}

function gm_write_data_auth(&$DATI)
{
	$answer   = [];
	$answer[] = gm_val($DATI, eGM_STRING) ." - User";
	$answer[] = gm_val($DATI, eGM_STRING) ." - Password";
	
	$answer = array_merge($answer, gm_write_data($DATI));
	return $answer;
}

/*----------------------------------------------------------------------------*/
/* END OF FILE */
