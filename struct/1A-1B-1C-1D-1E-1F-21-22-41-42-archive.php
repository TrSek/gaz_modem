<?php
require_once("obj/objects.php");
require_once("obj/store_tables.php");

// **********************************************************
// Citanie indexov archivov podla casu
// **********************************************************
function gm_search_hourly(&$DATI)
{
	$answer = [];
	$answer[] = gm_val($DATI, eGM_DATETIME_HOURLY) ." - Date/Time";
	return $answer;
}

function gm_search_daily(&$DATI)
{
	$answer = [];
	$answer[] = gm_val($DATI, eGM_DATETIME_DAILY) ." - Date/Time";
	return $answer;
}

function gm_search_answer_hourly(&$DATI)
{
	global $dt_mark;
	$answer = [];
	$answer[] = gm_val($DATI, eGM_DATETIME_HOURLY) ." - Date/Time";
	$answer[] = gm_val($DATI, eGM_DWORD) ." - Index";
	return $answer;
}

function gm_search_answer_daily(&$DATI)
{
	global $dt_mark;
	$answer = [];
	$answer[] = gm_val($DATI, eGM_DATETIME_DAILY) ." - Date/Time";
	$answer[] = gm_val($DATI, eGM_DWORD) ." - Index";
	return $answer;
}

// **********************************************************
// Citanie samotnych archivov podla indexu
// **********************************************************
function gm_archiv_hourly(&$DATI)
{
	$answer = [];
	$answer[] = gm_val($DATI, eGM_BYTE) ." - Count";
	$answer[] = gm_DP_Info( gm_val($DATI, eGM_WORD));
	$answer[] = gm_val($DATI, eGM_DWORD) ." - Index of value";
	return $answer;
}

function gm_archiv_hourly_old(&$DATI)
{
	$answer = [];
	$answer[] = gm_val($DATI, eGM_BYTE) ." - Count";
	$answer[] = gm_val($DATI, eGM_WORD) ." - Index of value";
	return $answer;
}

function gm_archiv_daily(&$DATI)
{
	$answer = [];
	$answer[] = gm_val($DATI, eGM_BYTE) ." - Count";
	$answer[] = gm_val($DATI, eGM_BYTE) ." - DP count";
	$answer[] = gm_DP_Info( gm_val($DATI, eGM_WORD));
	$answer[] = gm_val($DATI, eGM_DWORD) ." - Index of value";
	return $answer;
}

function gm_archiv_daily_old(&$DATI)
{
	$answer = [];
	$answer[] = gm_val($DATI, eGM_BYTE) ." - Count";
	$answer[] = gm_val($DATI, eGM_WORD) ." - Index of value";
	return $answer;
}

// hodinovy archiv vsetky hodnoty z jednej hodiny 0x1C
function gm_archiv_answer_hourly_old(&$DATI, $gm_funct)
{
	$answer    = [];
	$dp_table  = gm_DP1_arch();
	$count     = gm_val($DATI, eGM_BYTE);
	$dp_index  = gm_val($DATI, eGM_WORD);
	$answer[]  = $count ." - Count";
	
	while(($DATI != "") && $count--)
	{
		$dp_index  = gm_val($DATI, eGM_BYTE);
		$status    = gm_get_status( gm_val($DATI, eGM_BYTE));
		$value     = gm_val($DATI, $dp_table[$dp_index]['variable']);
		
		$answer[] = $value;
		$answer[] = "   -> ". gm_DP1_arch_Info($dp_index);
		
		if( !empty($status))
		{
			foreach ($status as $status_line) {
				$answer[] = "   -> ". $status_line;
			}
		}
	}
	return $answer;
}

// hodinovy archiv jedna hodnota po hodinach 0x1D
function gm_archiv_answer_hourly_from_old(&$DATI, $gm_funct)
{
	$dp_table  = gm_DP1_arch();
	$count     = gm_val($DATI, eGM_BYTE);
	$dp_index  = gm_val($DATI, eGM_WORD);
	$period    = gm_val($DATI, eGM_BYTE);
	$dtime     = gm_val($DATI, eGM_DATETIME_HOURLY);

	$answer    = [];
	$answer[]  = $count ." - Count";
	$answer[]  = $period ." - Period of measurement";
	$answer[]  = $dtime ." - Date/Time";

	$dtime = gm_TimeStamp($dtime);
	$val_index = 0;

	while(($DATI != "") && $count--)
	{
		$status = gm_get_status( gm_val($DATI, eGM_BYTE));
		$value  = gm_val($DATI, $dp_table[$dp_index]['variable']);

		$answer[] = $value;
		$answer[] = "   -> ". $val_index;
		$answer[] = "   -> ". gm_DP1_arch_Info($dp_index);
		$answer[] = "   -> ". date('Y-m-d H:i', $dtime);

		if( !empty($status))
		{
			foreach ($status as $status_line) {
				$answer[] = "   -> ". $status_line;
			}
		}

		$dtime -= $dtime % ($period * SEC_PER_MINUTE);
		$dtime = strtotime("+". $period ."minutes", $dtime);
		$val_index++;
	}
	return $answer;
}

function gm_archiv_answer_hourly(&$DATI, $gm_funct)
{
	$count     = gm_val($DATI, eGM_BYTE);
	$dp_index  = gm_val($DATI, eGM_WORD);
	$period    = gm_val($DATI, eGM_BYTE);
	$dtime     = gm_val($DATI, eGM_DATETIME_HOURLY);
	$val_index = gm_val($DATI, eGM_DWORD);
	
	$answer    = [];
	$answer[]  = $count ." - Count";
	$answer[]  = gm_DP_Info($dp_index);
	$answer[]  = $period ." - Period of measurement";
	$answer[]  = $dtime ." - Date/Time";
	$answer[]  = $val_index ." - Index of value";

	$dp_table  = gm_load_DP();
	$variable  = $dp_table[$dp_index]['variable'];
	if( empty( $variable ))
		$variable = eGM_LONG_REAL;

	$dtime = gm_TimeStamp($dtime);
	while(($DATI != "") && $count--)
	{
		$status = gm_get_status( gm_val($DATI, eGM_BYTE));
		$value  = gm_val($DATI, $variable);
		
		$answer[] = $value;
		$answer[] = "   -> ". $dp_table[$dp_index]['unit'];
		$answer[] = "   -> ". date('Y-m-d H:i', $dtime);
		$answer[] = "   -> ". $val_index;
		
		if( !empty($status))
		{
			foreach ($status as $status_line) {
				$answer[] = "   -> ". $status_line;
			}
		}
		
		$dtime -= $dtime % ($period * SEC_PER_MINUTE);
		$dtime = strtotime("+". $period ."minutes", $dtime);
		$val_index++;
	}
	return $answer;
}

function gm_archiv_answer_daily(&$DATI, $gm_funct)
{
	$count     = gm_val($DATI, eGM_BYTE);
	$dp_count  = gm_val($DATI, eGM_BYTE);
	$dp_index  = gm_val($DATI, eGM_WORD);
	$index     = gm_val($DATI, eGM_DWORD);
	
	$answer    = [];
	$answer[]  = $count ." - Count";
	$answer[]  = $dp_count ." - Count of DP";
	$answer[]  = gm_DP_Info($dp_index);
	$answer[]  = $index  ." - Index of value";
	$answer[]  = "";

	$dp_table  = gm_load_DP();
	while(($DATI != "") && $count--)
	{
		$dtime = gm_val($DATI, eGM_DATETIME_DAILY);
		for($iDP=0; $iDP<$dp_count; $iDP++)
		{
			$status = gm_get_status( gm_val($DATI, eGM_BYTE));
			$value  = gm_val($DATI, $dp_table[$dp_index]['variable']);
			
			$answer[] = $value ." ". $dp_table[$dp_index + $iDP]['unit'];
			$answer[] = "   -> ". gm_DP_Info($dp_index + $iDP);
			$answer[] = "   -> ". $dtime;
			
			if( !empty($status))
			{
				foreach ($status as $status_line) {
					$answer[] = "   -> ". $status_line;
				}
			}
		}
		$answer[] = "";
	}
	return $answer;
}


function gm_archiv_answer_daily_old(&$DATI, $gm_funct)
{
	$dp_table  = gm_DP1_arch();
	$count     = gm_val($DATI, eGM_BYTE);
	$dp_index  = gm_val($DATI, eGM_WORD);
	
	$variable = $dp_table[$dp_index]['variable'];
	if( $variable == eGM_LONG_REAL )
		$variable = eGM_MIDDLE_REAL;

	$answer    = [];
	$answer[]  = $count ." - Count";
	$answer[]  = $dp_index  ." - Index of value";
	$answer[]  = gm_val($DATI, eGM_DATETIME_DAILY) ." - Date/Time";
	$answer[]  = "";

	while(($DATI != "") && $count--)
	{
		$status = gm_get_status( gm_val($DATI, eGM_BYTE));
		$value  = gm_val($DATI, $variable);
		
		$answer[] = $value ." ". $dp_table[$dp_index]['unit'];
		$answer[] = "   -> ". gm_DP1_arch_Info($dp_index);
		
		if( !empty($status))
		{
			foreach ($status as $status_line) {
				$answer[] = "   -> ". $status_line;
			}
		}
	}
	return $answer;
}

/*----------------------------------------------------------------------------*/
/* END OF FILE */
