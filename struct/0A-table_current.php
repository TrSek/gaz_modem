<?php
require_once("obj/objects.php");
require_once("obj/store_tables.php");

function gm_ask_current(&$DATI)
{
	$answer = [];
	$answer[] = gm_val($DATI, eGM_BYTE) ." - Count of Data";
	$answer[] = gm_val($DATI, eGM_WORD) ." - Index";
	return $answer;
}

function gm_read_current(&$DATI)
{
	$answer = [];
	$dp_table = gm_load_DP1();

	$count    = gm_val($DATI, eGM_BYTE);
	$index_dp = gm_val($DATI, eGM_WORD);
	$answer[] = $count ." - Count of Data";
	$answer[] = $index_dp ." - Index";

	while( $count-- )
	{
		// posledny prvok je taky neutralny
		if( $index_dp >= count($dp_table))
			$index_dp = count($dp_table)-1;
		
		$variable = $dp_table[$index_dp]['variable'];
		$status   = gm_get_status( gm_val($DATI, eGM_BYTE));
		$value    = gm_val($DATI, $variable);

		$answer[] = "";
		$answer[] = $dp_table[$index_dp]['mark'] ." = ". $value ." ". $dp_table[$index_dp]['unit'];

		if( !empty($status))
		{
			foreach ($status as $status_line)
				$answer[] = "   -> ". $status_line;
		}
		$index_dp++;
	}

	return $answer;
}

/*----------------------------------------------------------------------------*/
/* END OF FILE */
