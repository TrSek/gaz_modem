<?php
require_once("obj/objects.php");

function gm_authorize_answer(&$DATI)
{
	$answer   = [];
	$answer[] = gm_val($DATI, eGM_STRING) ." - User";
	$answer[] = gm_val($DATI, eGM_STRING) ." - Password";

	return $answer;
}

/*----------------------------------------------------------------------------*/
/* END OF FILE */
