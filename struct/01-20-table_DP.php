<?php
require_once("obj/objects.php");
require_once("obj/store_tables.php");

function gm_read_dp_def(&$DATI)
{
	$answer = [];
	$answer[] = gm_val($DATI, eGM_BYTE) ." - Count of DP";
	$answer[] = gm_val($DATI, eGM_WORD) ." - Index";
	return $answer;
}

function gm_answer_dp_def(&$DATI)
{
	global $GM_VARIABLE_STR;
	$answer = [];

	$count = gm_val($DATI, eGM_BYTE);
	$dp_index = gm_val($DATI, eGM_WORD);
	$answer[] = $count ." - Count of DP";
	$answer[] = $dp_index ." - Index";
	$dp_table = gm_load_DP();
	$i = $dp_index;
	
	while( $count-- )
	{
		$mark  = gm_val($DATI, eGM_STRING);
		$unit  = gm_val($DATI, eGM_STRING);
		$expon = gm_val($DATI, eGM_BYTE);
		$info  = hexdec(substr_cut($DATI, 1));
		$dp_table[$i++] = array('mark' => $mark, 'unit' => $unit, 'exponent' => $expon, 'variable' => $info & 0x0F, 'info' => $info);

		$info_str = "";
		if( $info & 0x10 ) $info_str .= (empty($info_str)? "": ", ") ."read";
		if( $info & 0x20 ) $info_str .= (empty($info_str)? "": ", ") ."write";
		if( $info & 0x40 ) $info_str .= (empty($info_str)? "": ", ") ."hourly";
		if( $info & 0x80 ) $info_str .= (empty($info_str)? "": ", ") ."daily";
		
		$answer[] = $mark;
		$answer[] = "   -> ". $unit;
		$answer[] = "   -> ". $expon;
		$answer[] = "   -> ". $GM_VARIABLE_STR[$info & 0x0F];
		$answer[] = "   -> ". $info_str;
	}

	gm_save_DP($dp_table);
	return $answer;
}

function gm_answer_dp_detail(&$DATI)
{
	$answer = [];

	$count = gm_val($DATI, eGM_BYTE);
	$index = gm_val($DATI, eGM_WORD);
	$answer[] = $count ." - Count of DP";
	$answer[] = $index ." - Index";
	$answer[] = "";
	
	while( $count-- )
	{
		$answer[] = $index++ ." - ". gm_val($DATI, eGM_STRING);
	}
	return $answer;
}

/*----------------------------------------------------------------------------*/
/* END OF FILE */
