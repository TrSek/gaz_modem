<?php
require_once("obj/objects.php");

function gm_ask_alarm(&$DATI)
{
	$answer   = [];
	$answer[] = gm_val($DATI, eGM_BYTE) ." - Count of Data";
	$answer[] = gm_val($DATI, eGM_WORD) ." - Index";
	return $answer;
}

function gm_read_alarm(&$DATI)
{
	$answer = [];
	$count = gm_val($DATI, eGM_BYTE);
	$index = gm_val($DATI, eGM_WORD);
	$answer[]  = $count ." - Count";
	$answer[]  = $index ." - Dynamics Index";
	
	while( $DATI != "" )
	{
		$kod         = gm_val($DATI, eGM_BYTE);
		$dtime_start = gm_val($DATI, eGM_DATETIME);
		$dtime_stop  = gm_val($DATI, eGM_DATETIME);
		$status      = gm_get_status( gm_val($DATI, eGM_BYTE));
		$value       = gm_val($DATI, eGM_SHORT_REAL);
		
		$answer[] = $kod;
		$answer[] = "   -> ". $dtime_start;
		$answer[] = "   -> ". $dtime_stop;
		$answer[] = "   -> Vn = ". $value;
		
		if( !empty($status))
		{
			foreach ($status as $status_line) {
				$answer[] = "     -> ". $status_line;
			}
		}
		$answer[] = "";
	}
	
	return $answer;
}

/*----------------------------------------------------------------------------*/
/* END OF FILE */
