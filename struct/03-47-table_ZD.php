<?php
require_once("obj/objects.php");
require_once("obj/store_tables.php");

function gm_read_zd_def(&$DATI)
{
	$answer = [];
	$answer[] = gm_val($DATI, eGM_BYTE) ." - Count of ZD";
	$answer[] = gm_val($DATI, eGM_WORD) ." - Index";
	return $answer;
}

function gm_answer_zd_def(&$DATI)
{
	$answer = [];
	$dp_table = gm_load_DP();
	$zd_table = gm_load_ZD();

	$count = gm_val($DATI, eGM_BYTE);
	$answer[] = $count ." - Count of ZD";
	$answer[] = gm_val($DATI, eGM_WORD) ." - Index";
	
	while( $count-- )
	{
		$code = gm_val($DATI, eGM_BYTE);
		$mark = gm_val($DATI, eGM_STRING);
		$parc = gm_val($DATI, eGM_BYTE);
		
		$zd_table[$code]['mark'] = $mark;
		$zd_table[$code]['dp'] = [];
		$i = 0;
		
		$answer[] = $code ." - ". $mark ." (". $parc .")";
		while( $parc-- )
		{
			$dp_index = gm_val($DATI, eGM_WORD);
			$zd_table[$code]['dp'][$i++] = $dp_index;
			
			$answer[] = "  -> ". gm_DP_Info($dp_index);
		} 
	}

	gm_save_ZD($zd_table);
	return $answer;
}

function gm_ask_zd_index(&$DATI)
{
	$answer = [];
	$answer[] = gm_val($DATI, eGM_BYTE) ." - Count of ZD";
	$answer[] = gm_val($DATI, eGM_DWORD) ." - Index";
	return $answer;
}

function gm_read_zd(&$DATI)
{
	$dp_table = gm_load_DP();
	$zd_table = gm_load_ZD();

	$count    = gm_val($DATI, eGM_BYTE);
	$zp_index = gm_val($DATI, eGM_WORD);

	$answer = [];
	$answer[] = $count ." - Count";
	$answer[] = $zp_index ." - Dynamics Index";
	$answer[] = "";

	while( $DATI != "" )
	{
		$kod         = gm_val($DATI, eGM_BYTE);
		$dtime_start = gm_val($DATI, eGM_DATETIME);
		$dtime_stop  = gm_val($DATI, eGM_DATETIME);

		$answer[] = gm_ZD_Info($kod);
		$answer[] = "   -> ". $dtime_start;
		$answer[] = "   -> ". $dtime_stop;

		foreach ($zd_table[$kod]['dp'] as $dp_index)
		{
			$status = gm_get_status( gm_val($DATI, eGM_BYTE));
			$value  = gm_val($DATI, $dp_table[$dp_index]['variable']);

			$answer[] = "   ". $dp_table[$dp_index]['mark'] ." = ". $value ." ". $dp_table[$dp_index]['unit'];

			if( !empty($status))
			{
				foreach ($status as $status_line) {
					$answer[] = "     -> ". $status_line;
				}
			}
		}

		$answer[] = "";
	}
	return $answer;
}

function gm_read_zd_index(&$DATI)
{
	$dp_table  = gm_load_DP();
	$zd_table  = gm_load_ZD();
	
	$count     = gm_val($DATI, eGM_BYTE);
	$zp_index  = gm_val($DATI, eGM_WORD);
	$zp_index2 = gm_val($DATI, eGM_DWORD);
	
	$answer = [];
	$answer[] = $count ." - Count";
	$answer[] = $zp_index ." - Dynamics Index";
	$answer[] = $zp_index2 ." - Index";
	$answer[] = "";

	while( $DATI != "" )
	{
		$kod         = gm_val($DATI, eGM_BYTE);
		$dtime_start = gm_val($DATI, eGM_DATETIME);
		$dtime_stop  = gm_val($DATI, eGM_DATETIME);
		
		$answer[] = gm_ZD_Info($kod);
		$answer[] = "   -> ". $dtime_start;
		$answer[] = "   -> ". $dtime_stop;
		
		foreach ($zd_table[$kod]['dp'] as $dp_index)
		{
			$status = gm_get_status( gm_val($DATI, eGM_BYTE));
			$value  = gm_val($DATI, $dp_table[$dp_index]['variable']);

			$answer[] = "   ". $dp_table[$dp_index]['mark'] ." = ". $value ." ". $dp_table[$dp_index]['unit'];

			if( !empty($status))
			{
				foreach ($status as $status_line) {
					$answer[] = "     -> ". $status_line;
				}
			}
		}
		
		$answer[] = "";
	}
	return $answer;
}

/*----------------------------------------------------------------------------*/
/* END OF FILE */
