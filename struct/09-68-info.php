<?php
require_once("obj/objects.php");

function gm_info_short(&$DATI)
{
	$answer = [];
	$answer[] = gm_val($DATI, eGM_STRING) ." - Manufacture";
	$answer[] = gm_val($DATI, eGM_STRING) ." - Device name";
	$answer[] = gm_val($DATI, eGM_STRING) ." - Serial number";
	$answer[] = gm_val($DATI, eGM_STRING) ." - Manufacture identification";
	$answer[] = gm_val($DATI, eGM_STRING);
	$answer[] = gm_val($DATI, eGM_STRING);

	return $answer;
}

function gm_info(&$DATI)
{
	$answer = [];
	$answer[] = "";
	return $answer;
}

function gm_info_old2(&$DATI)
{
	$answer = [];
	$answer[] = gm_val($DATI, eGM_STRING) ." - Time";
	$answer[] = gm_val($DATI, eGM_STRING) ." - Pressure";
	$answer[] = gm_val($DATI, eGM_STRING) ." - Reserve1";
	$answer[] = gm_val($DATI, eGM_STRING) ." - Reserve2";
	$answer[] = gm_val($DATI, eGM_STRING) ." - Temperature";
	$answer[] = gm_val($DATI, eGM_STRING) ." - Gaz type";
	return $answer;
}

/*----------------------------------------------------------------------------*/
/* END OF FILE */
